Go back to [README.md](README.md)

# Docker
*Docker is a computer program that performs operating-system-level virtualization, also known as "containerization". ... All containers are run by a single operating-system kernel and are thus more lightweight than virtual machines. Containers are created from "images" that specify their precise contents. [Read more](https://docs.docker.com/)*

With Docker we can run a WordPress site on a localhost. This is how you get it running.

### Install Docker
- First, install/update [Node and npm](https://nodejs.org/en/)  
- Then, install [Docker for mac](https://download.docker.com/mac/stable/Docker.dmg) or [Docker for win10](https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe)

After Docker is installed confirm that Docker is running by opening the installed Docker App.


### Run Docker
In the root of this project there is a file named `example.env`. Copy this file and rename to `.env`.

This file will be used by `docker-compose.yml`. This file contains the necessary settings to run the website in Docker.

Then go to your Terminal/Console and run the following command from the project root:

```console
docker-compose up -d
```

If there are no conflicting ports in your local environment everything should work now. And you should be able to visit `localhost:[your-chosen-port]` to see the wp install running

You should also be able to visit `localhost:[your-chosen-port]` to open phpMyAdmin

If Not. then you need to change the port numbers from the "docker-compose.yml" file.

## Bonus
Use [Kitematic](https://kitematic.com) to manage your docker images
