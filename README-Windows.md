Go back to [README.md](README.md)
# Prerequisites

Before you begin, make sure you have administrative access to your computer system. Also, check if your system meets the hardware requirements for running Docker and WSL2.

## Installing WSL 2

1.  Open PowerShell as an administrator.
2.  Run the following command to enable the Windows Subsystem for Linux feature:


`dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`

3.  Run the following command to enable the Virtual Machine Platform feature:

`dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`

4.  Restart your computer to apply the changes.
5.  After the restart, download and install the latest WSL 2 Linux kernel from Microsoft: [https://aka.ms/wsl2kernel](https://aka.ms/wsl2kernel).

## Installing Ubuntu on WSL 2
### Step 1: Download Ubuntu

1.  Open the **Microsoft Store** app.
2.  Search for **"Ubuntu"**.
3.  Select **"Ubuntu 20.04 LTS"** (or later) from the list of results.
4.  Click **"Get"** and wait for the installation to complete.

### Step 2: Install Ubuntu

1.  Open the **Start** menu and search for **"Ubuntu"**.
2.  Select **"Ubuntu 20.04 LTS"** (or later) from the list of results.
3.  Wait for the installation process to complete.
4.  When prompted, create a new username and password for your Ubuntu installation.

### Step 3: Update Ubuntu

1.  Open a terminal window by pressing **Ctrl+Alt+T**.
2.  Run the following command to update Ubuntu:

`sudo apt update && sudo apt upgrade`

3.  Enter your password when prompted and wait for the update to complete.

## Installing nvm on Ubuntu on WSL 2

1.  Open the Ubuntu app.
2.  Update the package list by running the following command:

`sudo apt update`

3.  Install nvm by running the following command:

`curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash`

4.  Close and reopen the terminal window.
5.  Install the latest version of Node.js using nvm:

`nvm install node`

## Installing Docker

### For Windows

1.  Go to the official Docker website ([https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)) and download the Docker Desktop for Windows installer.
2.  Double-click the downloaded file to launch the installer.
3.  Follow the on-screen instructions to install Docker. During the installation process, you'll be prompted to enable Hyper-V and restart your computer.
4.  After the restart, Docker should be up and running.

### For Linux

1.  Open the Ubuntu app.
2.  Install Docker by running the following command:

`sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io`

3.  Add your user to the `docker` group:

`sudo usermod -aG docker $USER`

4.  Restart your computer to apply the group changes.

### Install PHP for Windows
1. go to https://www.php.net/downloads
2. click on "windows download" in the "current stable PHP..." section.
3. press on the zip button in the "Thread Safe" section.
4. when the download is finished you can select the zip file. then right click on the zipfile and press "extract all".
5. move this file to the C drive.
6. open this file and copy the path of the file.
7. type in your windows search "edit the system environment variables for your account".
8. click on "path" in current user if you want to add php for the current user. if you want to add php to the whole system then do the same but in the system variables table.
9. click on edit.
10. paste the file path on a new line (the one you copied in step 6).
11. to check if it workd open your command prompt and type: php --version.



# Set up Personal SSH Keys on Linux

SSH keys are a secure way of logging into a Linux or Unix-based server. This guide shows you how to set up and use SSH keys on a Linux or Unix-based system.

## Prerequisites

To follow this tutorial, you'll need the following:

-   A Linux or Unix-based system (e.g. Ubuntu, CentOS, Debian, macOS, or FreeBSD).
-   A command-line interface (e.g. Terminal or Git Bash).
-   Basic knowledge of the Linux or Unix command line.
-   A Bitbucket account.

## Step 1: Check for existing SSH keys

1.  Open a terminal window.
2.  Check for existing SSH keys on your system by running the following command:


`ls -al ~/.ssh`

3.  If you see a list of files with names like `id_rsa.pub` or `id_dsa.pub`, you already have an SSH key pair. Skip to Step 2.

4.  If you don't see any files with names like `id_rsa.pub` or `id_dsa.pub`, you need to create a new SSH key pair. Move on to Step 2.


## Step 2: Create a new SSH key pair

1.  Open a terminal window.
2.  Generate a new SSH key pair by running the following command:



`ssh-keygen -t rsa -b 4096 -C "your_email@example.com"`

3.  Press enter to accept the default file location and name.
4.  Enter a secure passphrase when prompted.
5.  You now have a new SSH key pair. Run the following command to display the public key:



`cat ~/.ssh/id_rsa.pub`

6.  Copy the entire public key to your clipboard.

## Step 3: Add the SSH key to Bitbucket

1.  Log in to your Bitbucket account.
2.  Click on your profile picture in the bottom-left corner of the page, then select "Bitbucket settings".
3.  Click on "SSH keys" in the left-hand navigation menu.
4.  Click "Add key".
5.  Enter a Label for the key (e.g. "My Linux Laptop").
6.  Paste the public key you copied in Step 2 into the "SSH Key" field.
7.  Click "Add key".

## Step 4: Test the SSH key

1.  Open a terminal window.
2.  Test the SSH key by running the following command, replacing `your_username` with your Bitbucket username and `your_bitbucket_account` with your Bitbucket account:


`ssh -T git@bitbucket.org -p 22 -vvv`

3.  If the key is set up correctly, you should see a message that says "logged in as username".
4.  If you see an error message, double-check that you've followed all the steps correctly.

## Install the wp-takeoff CLI

The wp-takeoff CLI is a command line interface for the wp-takeoff starterkit. It allows you to install the starterkit and manage your WordPress projects.

1. Open the Ubuntu app on your Windows 10 computer.
2. Open a terminal window by pressing `Ctrl + Alt + T`.
3. Run the following command to clone the repository:

`git clone https://bitbucket.org/labelvier/wp-takeoff-cli.git`

This will clone the repository to a new directory named `wp-takeoff-cli` in the current directory.

4. If prompted, enter your Bitbucket username and password.

   Note: If you have set up SSH keys for your Bitbucket account, you can use SSH instead of HTTPS to clone the repository. To do this, replace the HTTPS URL in the `git clone` command with the SSH URL.

`git clone git@bitbucket.org:labelvier/wp-takeoff-cli.git`

5. Install the core for the current repository by running the following command:

`./wp-takeoff core install`

6. if this doesn't work because it adds ^m behind /bin/bash. try the following command:

`sudo apt install dos2unix` + `dos2unix wp-takeoff inc/*.sh` + `dos2unix .env`

after this repeat step 5.

7. After the installation you get the option to choose which $PATH variable you want to set and choose the first option:

`bash`

8. Install the WordPress starterkit in the current repository by running the following command:

`wp-takeoff starterkit install`

9. Start docker from the project root by running the following command (from your window terminal):

`docker-compose up -d`

10. Cd to your project and run the following command in the Ubuntu terminal:

`nvm install lts`

11. Install the latest lts version (type `nvm ls-remote` to see the latest version):

`nvm install <last version>`

12. Remove the current node_modules by running this command:

`rm -rf wp-content/themes/<your theme name>/node_modules/`

13. Run the following command in the Ubuntu terminal from the project root to install the npm packages:

`npm install`

14. Run the following command in Ubuntu app from the project root to run the repository:

`npm run dev`

## Bonus
To open the folder where the repository is cloned, you can go to the File Explorer -> Linux -> Ubuntu -> home -> your name -> and find the name of the repository. In this way you can open the folder in File Explorer and edit the files in the repository with VS Code of any other editor.