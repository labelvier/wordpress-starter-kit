# LABEL VIER Project
This WordPress project runs in Docker. To start, check the [README-Docker.md](README-Docker.md).

The Label Vier Starterkit is a WordPress scaffolding tool designed for WordPress projects.
It offers multiple features that can be used throughout the site. Thanks to its modular setup, features can be easily added or removed without breaking the theme.
The starter kit consists of three main sections: header, main, and footer:

- **Header**: Contains the site's branding, navigation, and other elements that appear at the top of each page.
- **Main**: Contains the main content of the site, such as posts, pages, and custom post types.
- **Footer**: Contains elements that appear at the bottom of each page, such as copyright information, social media links, and contact details.

The theme is structured into the following folders:

- `_library`:
  Contains custom or third-party libraries that are not available through package managers like Composer. This folder may include reusable PHP classes, helper functions, and other code essential for the theme.
- `acf-json`:
  Storage for JSON files of Advanced Custom Fields (ACF). This allows field configurations to be easily managed and migrated between different environments, enabling version control and consistency for ACF field groups.
- `inc`:
  This folder contains modular PHP files for specific functionalities, such as:
    - Custom post types.
    - Settings for the theme customizer.

      Each file handles a specific aspect of the theme, keeping the code organized and modular.
- `languages`:
  Contains language files in .mo or .po format for translating text strings within the theme. This makes the theme suitable for multilingual applications.
- `src`:
  Stores source files, such as:
    -  **JavaScript**: For logic and interactivity, often bundled and minified.
    - **SCSS**: For styling, compiled into CSS.
    - **Images**: Optimized for web use.

  These assets are processed by build tools like Webpack or Gulp and then stored in a production-ready folder, such as dist or build.
- `template-parts`:
  A repository for reusable template parts, such as the header, footer, or specific content sections. This structure makes the theme modular and maintainable.


## Setup WordPress
The first time you start this project in Docker WordPress will help you setup and start with clean installation. You can just follow the Wordpress installation wizard.

We prefer that you use the following username and password for WordPress.

```console
user: admin
password: password
```

## Getting started:
1. Copy `example.env` to `.env` and choose your default folders and ports. (see  [README-Docker.md](README-Docker.md))
2. Run (only the first time) `npm install`
3. (optional) Run `npm run db:import` to import the default db
4. (optional) Rename the `labelvier` theme folder to your projects name (set in the `.env` file)
5. Run `npm run start:dev` which starts the Docker image and executes the develop environment with auto-reloading.
   1. Alternatively you can run `docker-compose up` in 1 terminal.
   2. And `npm run dev` in another terminal.
6. Run `npm run stop` to stop the Docker image.

### Selecting the theme
In the wp-admin, go to Appearance/Themes and select the right theme. Now the website will work properly.

### Development
`npm run dev` to bundle the latest styling and scripts and put them in `/dist` in the theme-root.

This will also copy everything else in `/src` (except `/js` and `/scss`) to `/dist`.

When building the theme for production `/src` will be left out.

### Production
- Increase the version number in `scss/style.scss`
- `npm run build`, this will create a clean copy of the theme without the node_modules etc. The clean copy is placed in the root of the project in the folder `build`. All scripts and styling will be minified.

### Staging / Production Deploy
- Add deploy server details to .env file $DEPLOY_NAME, $DEPLOY_SERVER and $DEPLOY_SERVER_PATH
#### Staging
- Run `npm run ssh-copy-id-staging` to store ssh credentials
- Run `npm run deploy-staging`, to deploy to this server. This will first run the build and then rsync the files to the server.
#### Production
- Run `npm run ssh-copy-id` to store ssh credentials
- Run `npm run deploy`, to deploy to this server. This will first run the build and then rsync the files to the server.

## Branches
For starters we work in the development branch. When testing we use the staging and master (=live) branch. When live, we prefer to work with feature-branches.

## DB management
For now we export a DB dump to `wp-db-dump`. When a second developer joins, he can import the DB from there. You can export and import using the following commands:
1. `npm run db:export`
2. `npm run db:import`

## Updating
When you want to update your wordpress, pull the update and recreate the updated docker images:

```console
docker-compose pull
docker-compose up -d
```

### That's it... Happy Coding'!'

## Version History
### Version 5.0.0 - 5 February 2025
- **Major Refactor**: **SCSS Upgrade**
  - Restructured and optimized files for more efficient loading.
  - Correctly implemented `@use "sass:map"`.
  - Moved mixins to a centralized SCSS mixins file.
  - Renamed and improved menu breakpoints and content spacers.
  - Fixed broken non-generated code.
  - Improved content block and styling compatibility.
- **New Feature**: **Monkey Menu Improvements**
  - Cleaned up `monkeyMenu.js`, using `const` for submenu states.
  - Introduced `handleMenuClick` function for better readability.
  - Added tab colors to `_monkey-menuDesktop.css` using `:focus`.
  - Adjusted hover behavior to tab navigation in comments.
  - General cleanup of the Monkey Menu code.
- **Bugfix**: Set default theme version to `0.0.1`.
- **Bugfix**: Fixed styling issues in ACF forms in the backend.
- **Bugfix**: Adjusted header trigger height to ensure correct image positioning.
- **Update**: **Gravityforms** updated from `2.9.1` to `2.9.2`.
- **Update**: **Yoast SEO** updated from `24.3` to `24.4`.
- **Update**: **Glightbox** dependency updated to `v3.3.1`.
- **Update**: **Node.js** updated to version `22`.

- **Refactor**: Improved footer item rendering for better clarity.
- **Refactor**: Improved ACF tweaks and field consistency.

### Version 4.10.1 - 18 December 2024
- **Bugfix**: Fixed wrong paths in scss files.

### Version 4.10.0 - 18 December 2024

- **New Feature**: Added `woocommerce schema.org` integration for products in the library.  
- **New Feature**: Mobile scroll-to-top functionality added in the lightbox.  
- **Bugfix**: Fixed import statements in JavaScript files.  
- **Bugfix**: Improved critical SCSS handling by cleaning imports and adding style adjustments.  
- **Bugfix**: Refactored button alignment and general header image issues.  
- **Bugfix**: Fixed naming inconsistencies for logo class (`top-inner__logo`).  
- **Performance**: Deferred and optimized CSS loading for better performance.  
- **Performance**: Updated Swiper JS configuration.  
- **Update**: **Gravityforms** updated to versions 2.9.1 and fixed compatibility issues.  
- **Update**: **Yoast SEO** updated to version 24.0.  
- **Update**: Other plugin updates for WordPress compatibility and security.  
- **Build**: Improved build scripts to differentiate errors and warnings for easier debugging.  

### Version 4.9.1 - 24 September 2024
- **Updated starter-kit-engine version**
- **Bugfixes**:
   - Adjusted button hover color.
   - Fixed default button alignment.
   - Resolved double line issues in search.
   - Corrected outdated bugfix and style bugfix for the header image fallback.
   - Typo and styling fixes.
- **Advanced Custom Fields (ACF)**: Improved clarity of the image replacement functionality.
- **Plugin Updates**: Multiple plugin updates for `gravityforms` (2.8.x versions) and other plugins.


### 4.9.0 - 26 June 2024
* Swiper JS is deferred via intersection observer to improve performance
* Critical CSS is preloaded in the head and the rest of the css will be deferred.
* header img changed loading false to  'fetchpriority' => 'high'
* fixed ul ol styling
* added comment to fix import missing from compile
* import export db path now mariadb
* new default icons (mail/phone)
* Node version bump to 20

### 4.8.2 - 22 May 2024
* Icomoon fix
* upgrade WP CLI to 2.10.0
* Docker to PHP8.3
* New snipplet for acf form revisions

### 4.8.1 - 17 April 2024
* SCSS - Npm run without errors on first build
* Fix html escaping in selec2 for ACF

### 4.8.0 - 3 April 2024
* X icon instead of twitter icon
* Standard .env deploy variables for staging and production
* Gutenberg block styling

### 4.7.0 - 1 March 2024
* Small tweaks and bugfixes
* New deploy scripts
* flyout menu fixes for facetwp
* woocommerce support

### 4.6.0 - 12 May 2023
* Disallow file modifications by default, except when in development environment
* Added new performance rules to defer scripts and styles
* Added support for critical css
* Updated swiperjs to v9
* Default style tweaks
* Changed footer template and acf fields for better column support
* Improved child theme support
* New decoding attribute for header image
* Updated _library woocommerce php and scss 
* Updated docker to php 8.2

### 4.5.0 - 7 March 2023
* Added a Windows Readme
* Better npm run db:import and db:export container search
* Starter kit engine always on version 1 and browserlist to latest
* Moved woocommerce style deferrer to non-woocommerce pages only
* Fixed acf preview thumbnail entry
* Css and footer improvements
* Fix php8 fatal error for cookie-scripts

### 4.4.0 - 6 January 2023
* New cookie banner / popup with new method of script loading in the <head> and footer.
* Updated pipeline Node.js to v19
* Security fix, only allow manage_options to update theme settings
* New ticker in _library
* Monkeymenu and header tweaks
* Button radius and other scss parts now use css variables
* Added missing header archive template
* Refactor content-none.php
* New footer
* Fix npm run db:export reference to docker image
* new functions.scss file
* Added new wp cli function `npm run wp` to directly run wp cli commands
* Plugin and package updates

### 4.3.0 - 20 September 2022
* Added Dynamic Content Block
* remove unused files
* more precies calculation of top position
* blocks or content, not both
* more general use of form selector
* added dynamic primary and secondary color for gutenberg
* added dynamic primary and secondary color for gutenberg
* blocks or content, not both
* blocks or content, not both
* add content to page
* added mising search form
* removed trailing slash/
* button hover as loose partial
* added variation to container
* css variables
* hover as partial
* contentblock tweaks
* menu tweaking
* general style tweaks
* general style tweaks
* default post styling
* new footer column resizer
* new snipplets
* added button option for tmce
* added support for header image select for archive pages
* content block templates
* footer text/menu bugfix
* dc wrapper
* variable fix

### 4.2.0 - 7 September 2022
* Autoload files in inc folder (Austin)
* Double action / filter cleanup (Nathanael)
* Update dependencies / plugins (WP Renovate Bot)
* Monkeymenu update (Nathanael)
* Removed ACF Extended from the pre-installed plugins (Nathanael / Eric)

### 4.1.1 - 5 July 2022
* Fallback featured image refactoring now supports og:image

### 4.1.0 - 17 June 2022
* New custom header menu with dropdown (monkeymenu)
* Renovate updates (tests run on node 18 and swiper js)
* Label Vier plugin update to 1.4.3
* Password protect functionality added to the templates
* Facetwp filter fix for WP 6.0 multisites
* Enable auto upgrade for mariadb container

### 4.0.1 - 13 may 2022
* Anchor fix for safari

### 4.0.0 - 13 may 2022
* BREAKING - New Starter Kit engine (https://www.npmjs.com/package/@labelvier/starter-kit-engine)
* Added pipeline testing
* Added WP CLI support for docker image
* Removed example db dump file
* ACF popup preview for flexible layout
* scroll to content fix for Safari
* Performance tweaks for google lighthouse
* improve woocommerce not loading unnessesary files
* glightbox and swiper latest versions
* database export function fix
* added renovate bot
* button support for wysiwyg
* remove lazy loading image background color
* updated scss
* new utility classes
* refactor of the header.php
* fixed ACF error at login screen
* Updated WordPress Plugins
* Multisite support
* No lazy loading for header image
* Http check in library
* Gutter tweaks
* No results text for search
* Share icons example
* Dynamic blocks example in library

### 3.5.1 - 2 november 2021
* Added HSTS headers
* Gravity forms autocomplete fix
* Moved Woocommerce default styling to _library, enable if needed
* Labelvier plugin updated
* Removed relevanssi plugin

### 3.5.0 - 28 august 2021
* Added acf extended plugin
* Gutenberg / classic editor switch enhancements
* Cookie fix
* remove unused utilities
* added common used plugins
* template tweaks for new acf structure
* new header order
* rename site to page
* acf template fixes and content optimisations

### 3.4.3 - 29 july 2021
* Mobile menu script resize tweak
* Added default content sharing. Shows share links below single blog items.
* Cookie script typo fix
* Updated npm packages
* Added Woocommerce default styling to scss and disabled it from the plugin.

### 3.4.2 - 19 may 2021
* Added WORDPRESS_DB_USER in docker-compose

### 3.4.1 - 23 mar 2021
* Improved cookie select page

### 3.4.0 - 17 nov 2020
* Allow all gutenberg blocks
* Moved featured image ACF filter to the `_library`
* Shorter fade in animation for lazy loading
* Readme rewrite
* Facetwp via GET to use varnish caching (in `_library`)
* Deploy script via rsync to make the deploy faster and more reliable
* SwiperJS optimisations

### 3.3.0 - 23 sept 2020
* Header fixes
* tiny mce functions
* removed sidebar.php
* major gulp updates
* new menu html
* Footer settings in Theme settings
* bugfix - empty build bug
* removed auto restart Docker
* removed single post navigation
* gitignore fixes
* swiper via npm in parts
* child theme fixes


### 3.2.0 - 23 jun 2020
* labelvier plugin update to 1.0.0
* Lots of html tweaks
* Block fixes
* Title fixes
* ACF & Widget snippets
* Facetwp snippets
* Custom gutenberg color palette
* Menu rewrite

### 3.1.1 - 21 apr 2020
* warning in script `npm run deploy`
* Featured image block fix
* Fixed theme activation error
* Put cookie scripts outside div when they're not tracking cookies
* Plugin updates for ACF and Label Vier plugin

### 3.1.0 - 31 mar 2020
* Hide gutenberg on template switch
* unignore babelrc
* added loop info
* remove old bumping
* init is now install for clearification
* remove H1 from tinyMCE
* added default styling for swiper and gravity forms
* new custom image sizes
* added yoast breadcrumb tweak
* added some info for usage with FA kits
* added new way of activating plugins
* default numbered pagination
* added custom excerpt option
* node v12 fix

### 2.1.0 - 5 feb 2020
* Moved Lazy loading to Label Vier plugin
* Added filters to use or disable certain label vier plugin features
* Template/SCSS cleanup
* Static ACF Theme Settings now available under Appearance/Theme Settings (inc/site-options.php)
* Created _library folder for code examples
* Removed plugins which are not often used
* Added default ACF Content blocks
* Added ssh-copy and deploy script (add server details in .env and run deploy)

### 2.0.1 - 27 nov 2019
* Lazy load bugfix

### 2.0.0 - 22 oct 2019
* Rebuild gulp/webpack/yarn
* Restructured generated css/js/images
* Added ACF as default for theme settings

### 1.2.1 - 1 march 2019
* Updated styles from 80-days project

### 1.2.0 - 12 februari 2019
* Extra .env variables
* Favicon in the wp-login screen
* Woocommerce support
* FacetWP support
* Gutenberg disable option in ACF
* Docker to PHP 7.3 and latest Wordpress version

### 1.1.0 - 12 februari 2019
* Switched to Webpack (thanks to Sebas)

### 1.0.1 - 7 februari 2019
* Changed port to host to escape looping on default port 80

### 1.0.0 - 31 january 2019
* Published first version after lots of refactoring, cleaning etc by Nathanael
