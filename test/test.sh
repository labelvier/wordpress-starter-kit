#bin/bash

cp example.env .env
# set environment variables
export $(grep -o "^[^#]*" .env | xargs);

# go to the theme folder
cd wp-content/themes/"$THEME_FOLDER_NAME" || exit 1
# install dependencies
npm install

# run build, but this subcommand needs to cd into the theme folder
BUILD_OUTPUT=$(npm run build 2>&1)
FATAL_ERRORS=$(echo "$BUILD_OUTPUT" | grep -i "error")
WARNINGS=$(echo "$BUILD_OUTPUT" | grep -v "DEPRECATION WARNING: The legacy JS API is deprecated")
if [ -n "$FATAL_ERRORS" ]; then
  echo "build failed with fatal errors: ${FATAL_ERRORS}"
  exit 1
elif [ -n "$WARNINGS" ]; then
  echo "build succeeded with warnings: ${WARNINGS}"
else
  echo "build succeeded without warnings"
fi
