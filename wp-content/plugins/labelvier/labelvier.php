<?php
/*
Plugin Name: Label Vier Extras
Plugin URI: https://labelvier.nl
Description: Optimisations for the current wordpress theme.
Author: Label Vier
Version: 1.11.0
*/

/**
 * define plugin version
 */
define( 'LABELVIER_PLUGIN_VERSION', '1.11.0' );

//action schedular needs to be loaded before plugins_loaded => 0.
require_once( plugin_dir_path( __FILE__ ) . '/modules/action-scheduler/action-scheduler.php' );

/**
 * init all functions for the labelvier plugin, all functions can be suppressed with a filter
 */
function init_labelvier_plugin() {
	/**
	 * Enqueue scripts and styles.
	 */
	function labelvier_kit_scripts() {
		wp_enqueue_script( 'labelvier-starter-js', plugin_dir_url( __FILE__ ) . 'labelvier.js', array(), LABELVIER_PLUGIN_VERSION, true );
	}
	add_action( 'wp_enqueue_scripts', 'labelvier_kit_scripts' );

    /**
     * Enable labelvier custom admin dashboard
     */
    if ( apply_filters( 'labelvier/use_custom_admin_dashboard', false ) || (defined('LABELVIER_DASHBOARD') && LABELVIER_DASHBOARD === true) ) {
        require( __DIR__ . '/modules/custom-admin-dashboard/custom-admin-dashboard.php' );
    }

	/**
	 * Enable lazy loading
	 */
	if ( apply_filters( 'labelvier/use_lazy_loading', false ) ) {
		require( __DIR__ . '/modules/lazy-loading/create-lazy-image.php' );
	}

	/**
	 * Enable shortpixel compression
	 */
	if ( apply_filters( 'labelvier/use_shortpixel', true ) ) {
		require( __DIR__ . '/modules/shortpixel/shortpixel.php' );
	}

	/**
	 * Enable focal point functionality for thumbnails
	 */
	if ( apply_filters( 'labelvier/use_focal_point', false ) ) {
		require( __DIR__ . '/modules/focal-point/focal-point.php' );
	}

	/**
	 * Enable spam filter
	 */
	require( __DIR__ . '/modules/spam-filter/spam-filter-options.php' );
	$use_spam_filter = get_option('options_labelvier_enable_spam_filter', false);
	if ( apply_filters( 'labelvier/use_spam_filter',  $use_spam_filter) ) {
		require( __DIR__ . '/modules/spam-filter/spam-filter.php' );
	}

	/**
	 * Enable updates from bitbucket master branch
	 */
	require( __DIR__ . '/modules/update-checker/init.php' );

	/**
	 * Add ACF settings page
	 */
	require( __DIR__ . '/modules/site-options/site-options.php' );
	require( __DIR__ . '/modules/user-options/user-options.php' );

    /**
     * Profiler
     */
    require( __DIR__ . '/modules/l4-profiler/l4-profiler.php' );

    /**
     * Cache
     */
    require( __DIR__ . '/modules/cache/cache.php' );

    /**
     * Security
     */
    require( __DIR__ . '/modules/security/security.php' );

    /**
     * Force HTTPS
     */
    require( __DIR__ . '/modules/force-https/force-https.php' );

    /**
     * Enable user meta last logged in
     */
    if ( apply_filters( 'labelvier/use_user_meta_last_logged_in', true ) ) {
        require(__DIR__ . '/modules/user-meta-last-logged-in/user-meta-last-logged-in.php');
    }
}
add_action( 'after_setup_theme', 'init_labelvier_plugin', 10, 0 );
register_deactivation_hook( __FILE__, 'labelvier_remove_profiler_mu_plugin' );
