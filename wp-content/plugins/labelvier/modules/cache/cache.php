<?php
// if there is an acf option page update, run the hook
add_filter('acf/options_page/save', 'labelvier_reset_cache');

/**
 * Reset the cache if there is an acf option page update or a post update
 * @return void
 */
function labelvier_reset_cache() {
    if(function_exists('sg_cachepress_purge_everything')) {
        sg_cachepress_purge_everything();
    }
}
