<?php

if(isset($_GET['force_https'])) {
    add_action('init', function () {
        if(is_develop_environment()) {
            wp_die('Force HTTPS not added to .htaccess because we are in a develop environment');
        }
        labelvier_install_force_https();
        wp_die('Force HTTPS added to .htaccess');
    });
}

// check if we have a .htaccess file in the root of the wordpress installation
// run security tools after plugin label updates
add_action('upgrader_process_complete', function ($upgrader_object, $options) {
    if ($options['action'] == 'update' && $options['type'] === 'plugin') {
        foreach ($options['plugins'] as $plugin) {
            if ( str_contains( $plugin, 'labelvier' )) {
                labelvier_install_force_https();
            }
        }
    }
}, 10, 2);

// check if we have a .htaccess file in the root of the wordpress installation
add_action('wp_https_detection', 'labelvier_install_force_https');


// check if we have a .htaccess file in the root of the wordpress installation and if so, add the force https snippet
function labelvier_install_force_https() {
    if(is_develop_environment()) {
        return;
    }

    $htaccess = ABSPATH . '.htaccess';
    if (file_exists($htaccess)) {
        $content = file_get_contents($htaccess);
        if (strpos($content, '# BEGIN LABELVIER FORCE HTTPS ') === false && strpos($content, 'HTTPS forced by SG-Optimizer') === false) {
            $https_content = <<<EOT
# BEGIN LABELVIER FORCE HTTPS
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</IfModule>
# END LABELVIER FORCE HTTPS
EOT;
            $content = PHP_EOL . $https_content . PHP_EOL . PHP_EOL  . $content;
            file_put_contents($htaccess, $content);
        }
    }
}