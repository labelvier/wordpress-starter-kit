<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 17-01-2019
 * Time: 08:48
 */
?>
<div id="message" class="updated fade" style="display:none"></div>

<script type="text/javascript">
    // <![CDATA[

    function l4_profile() {
        $ = jQuery;
        var requestRepeat = 5;
        var currentPlugin = 0;
        var loadProfile = {};

        $("#l4_start_profiling").prop("disabled", true);

        <?php
        $plugins_js = "var plugins = [";
        $active_plugins = [];
        $network_plugins = get_site_option('active_sitewide_plugins', false);
        if($network_plugins !== false) {
            foreach($network_plugins as $path => $active_plugin) {
                $active_plugins[$path] = true;
            }
        }
	    foreach(get_option('active_plugins') as $active_plugin) {
		    $active_plugins[$active_plugin] = true;
	    }
        foreach($active_plugins as $active_plugin => $active) {
            $plugin_dir = explode('/', $active_plugin)[0];
            $plugins_js .= "'".$plugin_dir . "',";
        }

        echo $plugins_js .= "];";
            ?>

        console.log(plugins);

        //first do a global profile
        startProfiling(false, profileAllPlugins);

        //loop through all the plugins
        function profileAllPlugins() {
            var plugin = plugins[currentPlugin];
            if(!plugin) {
                return;
            }
            if(plugin.indexOf('l4-profiler') != -1 || plugin.indexOf('labelvier') != -1) {
                //skip to the next one
                currentPlugin++;
                profileAllPlugins();
                return;
            }
            //tmp try-out
            console.log('start profiling', plugin)
            startProfiling(plugin, function() {
                if(loadProfile[plugin].count != 0) {
                    let difference = round(loadProfile["all plugins"].average - loadProfile[plugin].average, 3);
                    update_log("request without " + plugin + " loads in " + loadProfile[plugin].average + "s average, making it " + Math.abs(difference) + "s " + (difference > 0 ? 'faster' : 'slower'));
                }
                currentPlugin++;
                profileAllPlugins();
                return;
            });
            return;
        }



        function startProfiling(plugin, callback) {
            plugin = plugin || false;
            var pluginLabel = plugin ? "with " + plugin + " disabled." : "with all plugins";
            update_log("Starting url load (" + requestRepeat + "x) "+pluginLabel+"... (" + $('#url').val() + ")");
            get_loading_time(plugin, callback);
        }

        function get_loading_time(plugin, callback) {
            plugin = plugin || "all plugins";
            if(typeof loadProfile[plugin] == "undefined") {
                loadProfile[plugin] = {
                    time : 0,
                    totalTime : 0,
                    count : 0,
                    average : 0
                }
            }
            var load = loadProfile[plugin];

            var ajaxTime= new Date().getTime();
            jQuery.ajax({
                url: $('#url').val(),
                data: {
                  disable_plugin : plugin
                },
                type: "GET",
                success: function (result) {
                    var totalTime = new Date().getTime()-ajaxTime;
                    //update_log("Loaded succesfully in " + (totalTime/1000) + "s");
                    load.count++;
                    load.totalTime += totalTime;
                    load.time = load.totalTime / load.count;
                    if(load.count == requestRepeat) {
                        load.average = round(load.totalTime / load.count / 1000, 3);

                        if(plugin == "all plugins") {
                            update_log("Average load in " + load.average + "s");
                        }

                        console.log(loadProfile);
                        //run callback function
                        callback();
                    } else {
                        //run this function again
                        get_loading_time(plugin, callback);
                    }
                    profiling_done();
                },
                error: function (request, status, error) {
                    update_log("Site crashes with this plugin disabled ("+status+" - " + error + ")");
                    callback();
                    profiling_done();
                }
            });
        }

        function profiling_done() {
            jQuery("#l4_start_profiling").prop("disabled", false);
        }

        function update_log(msg) {
            $('#p4-profile-log').val($('#p4-profile-log').val() + "\r\n" + msg);
            var textarea = document.getElementById('p4-profile-log');
            textarea.scrollTop = textarea.scrollHeight;
        }

        function round(value, decimals) {
            return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        }
    }


    // ]]>
</script>

<form method="post" action="" onsubmit="return false;" style="display:inline; ">
    <h4><?php _e( 'When you start the profiling, the plugin will tmp disable every active plugin one-by-one', 'l4-profiler' ); ?>:</h4>
    <p>
        <label>
            <input type="text" id="url" name="url" value="<?= home_url() ?>" style="width:90%" />
        </label>
    </p>

    <input type="button" onClick="javascript:l4_profile();" class="button" name="l4_start_profiling"
           id="l4_start_profiling"
           value="<?php _e( 'Start profiling', 'l4-profiler' ) ?>"/>
    <br/>

    <textarea id="p4-profile-log" style="width: 90%; height: 500px;"></textarea>
</form>
