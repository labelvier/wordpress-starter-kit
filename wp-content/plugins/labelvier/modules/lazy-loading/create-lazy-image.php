<?php
/**
 * Lazy image function file
 *
 * @package ibpix
 */

/**
 * Creates a images with enables lazy loading
 *
 * @package ibpix
 * @author Sebas & Eric
 *
 * @param string $id the icon id.
 * @param string $size the icon size.
 * @param string $sizes example: "(max-width: 640px) 100vw, 640px".
 * @return mixed html output.
 */
add_filter('wp_get_attachment_image_attributes', 'labelvier_create_lazy_image', 10, 3);
function labelvier_create_lazy_image($attr, $attachment, $size) {
	// don't do lazy loading in wp-admin
	if(is_admin()) {
		return $attr;
	}

	// don't do lazy loading on amp pages
	if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {
		return $attr;
	}

	// don't do lazy loading when filter is disabled
	if ( apply_filters( 'labelvier/use_lazy_loading', false ) === false ) {
		return $attr;
	}

    // respect fetchpriority attribute = high
    if( isset($attr['fetchpriority']) && $attr['fetchpriority'] === 'high') {
        return $attr;
    }

    // respect loading attribute
    if( isset($attr['loading']) && $attr['loading'] === false) {
        return $attr;
    }

	$id = $attachment->ID;

	//create svg placeholder
	$image   = wp_get_attachment_image_src( $id, $size );
	$w = $image[1];
	$h = $image[2];
	$srcset  = wp_get_attachment_image_srcset( $id, $size );
	$src_svg = 'data:image/svg+xml;utf8,<svg xmlns=&quot;http://www.w3.org/2000/svg&quot; xmlns:xlink=&quot;http://www.w3.org/1999/xlink&quot; version=&quot;1.1&quot; x=&quot;0px&quot; y=&quot;0px&quot; viewBox=&quot;0 0 '. $w .' '. $h .'&quot; xml:space=&quot;preserve&quot; height=&quot;'. $h .'px&quot; width=&quot;'. $w .'px&quot;></svg>';

	$attr['src'] = $src_svg;
	$attr['data-src'] = $image[0];
	$attr['data-srcset'] = $srcset;
	$attr['class'] .= " js-lazy anim-fadein";
	$attr['width'] = $image[1];
	$attr['height'] = $image[2];
	unset($attr['srcset']);
	// tmp fix to prevent lazy loading bug on safari 15.4
	unset($attr['loading']);

	return $attr;

}

// debugging for gutenberg
//add_filter('wp_get_attachment_metadata', function($data, $post_id) {
//	unset($data['sizes']);
//	return $data;
//}, 999, 2);
