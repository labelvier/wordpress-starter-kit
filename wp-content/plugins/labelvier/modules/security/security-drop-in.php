<?php
// Label Vier Security
// If request contains xmlrpc.php, deny
if(isset($_SERVER['REQUEST_URI'])) :
    if(!defined('LABELVIER_ENABLE_XMLRPC') &&  strpos($_SERVER['REQUEST_URI'], 'xmlrpc.php') !== false) {
        header('HTTP/1.0 403 Forbidden');
        die('Forbidden');
    }

    // Block request if called with a POST request and no referer and the request_uri contains wp-login.php
    if ( $_SERVER['REQUEST_METHOD'] === 'POST' &&
        strpos( $_SERVER['REQUEST_URI'], 'wp-login.php' ) !== false &&
        ( ! isset( $_SERVER['HTTP_REFERER'] ) || empty( $_SERVER['HTTP_REFERER'] ) || strpos( $_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST'] ) === false ) ) {
        header( 'HTTP/1.1 403 Forbidden' );
        die( 'Forbidden' );
    }

    // Woocommerce checks
    if ( $_SERVER['REQUEST_METHOD'] === 'POST' &&
        ( isset($_POST['woocommerce-register-nonce']) || isset($_POST['woocommerce-login-nonce']) || isset($_POST['woocommerce-lost-password-nonce']) ) &&
        ( ! isset( $_SERVER['HTTP_REFERER'] ) || empty( $_SERVER['HTTP_REFERER'] ) || strpos( $_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST'] ) === false ) ) {
        header( 'HTTP/1.1 403 Forbidden' );
        die( 'Forbidden' );
    }

    if ( $_SERVER['REQUEST_METHOD'] === 'POST' &&
        ( isset($_POST['woocommerce-register-nonce']) || isset($_POST['woocommerce-login-nonce']) || isset($_POST['woocommerce-lost-password-nonce']) ) &&
        ( !isset($_POST['valid_login']) || $_POST['valid_login'] !== date('Ymd') ) ) {
        header( 'HTTP/1.1 403 Forbidden' );
        die( 'Forbidden (missing valid login check)' );
    }


    // Block request if called with a POST request to wp-login.php and the HTTP_REFERER does not contain the string valid_referer
    // This is to prevent brute force attacks on the login form
    // disabled bot security checker since this also blocks a password protected page
    if ( $_SERVER['REQUEST_METHOD'] === 'POST' &&
        strpos( $_SERVER['REQUEST_URI'], 'wp-login.php' ) !== false &&
        ( !isset($_POST['valid_login']) || $_POST['valid_login'] !== date('Ymd') ) ) {
        header( 'HTTP/1.1 403 Forbidden' );
        die( 'Forbidden (missing valid login check)' );
    }
endif;

