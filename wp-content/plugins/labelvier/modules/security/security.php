<?php
// Label Vier Security
if(isset($_GET['install_security_tools'])) {
    labelvier_install_security_tools();
    wp_die('Security blocker installed.');
}

add_action('puc_cron_check_updates-labelvier', function () {
    labelvier_install_security_tools();
});

// run security tools after plugin label updates
add_action('upgrader_process_complete', function ($upgrader_object, $options) {
    if ($options['action'] == 'update' && $options['type'] === 'plugin') {
        foreach ($options['plugins'] as $plugin) {
            if ( str_contains( $plugin, 'labelvier' )) {
                labelvier_install_security_tools();
            }
        }
    }
}, 10, 2);

function labelvier_install_security_tools() {
    $security_block_start = "### Label Vier Security ###";
    $security_block_end = "### End Label Vier Security ###";
    // include the security.php file from security/ folder
    $current_path = __DIR__ . '/security-drop-in.php';
    // check if file exists
    $security_blocker = "if(file_exists('$current_path')) { require_once '$current_path'; }";
    $security_blocker = "$security_block_start
$security_blocker
$security_block_end";
    // check if the security block is already in wp-config.php
    $security_blocker_already_installed = strpos(file_get_contents(ABSPATH . 'wp-config.php'), $security_block_start);
    if($security_blocker_already_installed !== false) {
        // remove everything between ### Label Vier Security ### and ### End Label Vier Security ###
        $security_blocker_already_installed = strpos(file_get_contents(ABSPATH . 'wp-config.php'), $security_block_start);
        // get the last occurence of ### End Label Vier Security ###
        $security_blocker_already_installed_end = strrpos(file_get_contents(ABSPATH . 'wp-config.php'), $security_block_end);
        $security_blocker_already_installed_length = $security_blocker_already_installed_end - $security_blocker_already_installed + strlen($security_block_end);
        // get the contents in wp-config.php
        $security_blocker_already_installed = substr(file_get_contents(ABSPATH . 'wp-config.php'), $security_blocker_already_installed, $security_blocker_already_installed_length);
        // remove the security block from wp-config.php
        $security_blocker_already_installed = str_replace($security_blocker_already_installed, '', file_get_contents(ABSPATH . 'wp-config.php'));
        file_put_contents(ABSPATH . 'wp-config.php', $security_blocker_already_installed);
    }

    // add the security block to wp-config.php
    $handle = fopen(ABSPATH . 'wp-config.php', 'a');
    fwrite($handle, $security_blocker);
    fclose($handle);
}

// add a snippet of javascript to the wp-login page
// this snippet will add a hidden field to the login form with a value of the current date
// this value will be used to check if the referer is valid
function labelvier_login_form($echo = true) {
    $output = '<script type="text/javascript">
	console.log("Label Vier Security: Adding hidden field to the wp-login form.");
        // when dom is ready
        document.addEventListener("DOMContentLoaded", function() {
			// Label Vier Security: Adding hidden field to the wp-login form.
			// add a hidden field to the login form
			var today = new Date();
			var dd = today.getDate().toString();
			var mm = (today.getMonth()+1).toString(); //January is 0!
			var yyyy = today.getFullYear().toString();
			if(dd<10){dd=\'0\'+dd} if(mm<10){mm=\'0\'+mm} today = yyyy+mm+dd;
			var valid_login = document.createElement("input");
			valid_login.setAttribute("type", "hidden");
			valid_login.setAttribute("name", "valid_login");
			valid_login.setAttribute("value", today);
	        // get the form inside the div with id login
//	        document.querySelector("form").appendChild(valid_login);
			// add the hidden field to all forms on the page
			var forms = document.querySelectorAll("form");
            for(var i = 0; i < forms.length; i++) {
				forms[i].appendChild(valid_login.cloneNode(true));
			}
		});
	</script>';
    if($echo) {
        echo $output;
    } else {
        return $output;
    }
}
// add the snippet to the wp-login page
add_action('login_head', function() {
    labelvier_login_form(true);
});
// add a snippet of javascript to the password protected page
add_action('wp_head', function() {
    global $post;
    if(post_password_required($post)) {
        labelvier_login_form(true);
    }
}, 10, 2);

// add the snippet to the woocommerce page
add_action('woocommerce_login_form_start', function() {
    labelvier_login_form(true);
});
add_action('woocommerce_register_form_start', function() {
    labelvier_login_form(true);
});
add_action('woocommerce_resetpassword_form', function() {
    labelvier_login_form(true);
});
add_action('woocommerce_before_lost_password_form', function() {
    labelvier_login_form(true);
});

// disable the wp-json users endpoint, to enable this endpoint again, add LABELVIER_ENABLE_USERS_ENDPOINT to wp-config.php
add_filter('rest_endpoints', static function($endpoints) {
    // check if the constant is defined
    if ( ! defined( 'LABELVIER_ENABLE_USERS_ENDPOINT' ) ) {
        define( 'LABELVIER_ENABLE_USERS_ENDPOINT', false );
    }
    // check if the constant is true and return the endpoints if it is
    if ( LABELVIER_ENABLE_USERS_ENDPOINT ) {
        return $endpoints;
    }

    // disable the users endpoint
    if ( isset( $endpoints['/wp/v2/users'] ) ) {
        unset( $endpoints['/wp/v2/users'] );
    }
    // disable the user details endpoint
    if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
        unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
    }
    return $endpoints;
});