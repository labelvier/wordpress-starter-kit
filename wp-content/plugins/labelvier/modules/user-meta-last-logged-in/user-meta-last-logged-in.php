<?php


/**
 * Add last login info to user data
 */
function tlb_add_last_login( $user_login, $user ) {
	update_user_meta( $user->ID, 'last_login', time() );
}

add_action( 'wp_login', 'tlb_add_last_login', 10, 2 );

/**
 * Add Last Login column to user list
 */
function tlb_add_last_login_column( $columns ) {
	$columns['last_login'] = 'Last Login';

	return $columns;
}

add_filter( 'manage_users_columns', 'tlb_add_last_login_column' );

/**
 * Show last logged in date in Last Login column
 */
function tlb_add_last_login_column_content( $value, $column_name, $user_id ) {
	if ( 'last_login' === $column_name ) {
		$last_login = get_user_meta( $user_id, 'last_login', true );
		if ( $last_login ) {
			// echo $last_login (timestamp) to Wordpress date and time, and time zone
			$date_format = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
			$value       = wp_date( $date_format, $last_login );
		} else {
			$value = 'Unknown';
		}
	}

	return $value;
}

add_filter( 'manage_users_custom_column', 'tlb_add_last_login_column_content', 10, 3 );

/**
 * Make Last Login column sortable
 */
function tlb_add_last_login_column_sortable( $columns ) {
	$columns['last_login'] = 'last_login';

	return $columns;
}

add_filter( 'manage_users_sortable_columns', 'tlb_add_last_login_column_sortable' );

/**
 * If orderby=last_login is set, sort by meta_value_num
 */
function tlb_sort_last_login_column( $query ) {
	if ( $query->get( 'orderby' ) === 'last_login' ) {
		$meta_query = array(
			'relation' => 'OR',
			array(
				'key'     => 'last_login',
				'compare' => 'NOT EXISTS',
			),
			array(
				'key'     => 'last_login',
				'compare' => '=',
				'value'   => '',
			),
			array(
				'key'     => 'last_login',
				'compare' => 'EXISTS',
			),
		);
		if ( 'last_login' === $query->get( 'orderby' ) ) {
			$query->set( 'meta_query', $meta_query );
			$query->set( 'orderby', 'meta_value_num' );
		} else {
			$query->set( 'meta_query', $meta_query );
		}
	}
}

add_action( 'pre_get_users', 'tlb_sort_last_login_column' );
