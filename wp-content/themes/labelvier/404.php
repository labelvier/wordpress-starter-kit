<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Labelvier
 */

get_header();
?>

    <div class="o-container">

        <?= get_field('pages', 'option')['404_content'] ?>

    </div>

<?php
get_footer();
