document.addEventListener('DOMContentLoaded', function() {
    const contactForm7 = document.querySelector('.wpcf7-form');
    if (contactForm7) {
        contactForm7.addEventListener('wpcf7mailsent', function(event) {
            const thankYou = document.querySelector('.wpcf7-response-output');
            window.scrollTo(0, thankYou.offsetTop);
        }, false);
    }
});
