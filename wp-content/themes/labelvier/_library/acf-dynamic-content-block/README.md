Put the .json files into the acf-json folder of the theme and import them in the admin.

Put the .scss in scss/f-templates

And put the .php files into the template-parts/blocks folder.

Add a flexible content layout to Content Blocks and name it dynamic-content. Now make a clone to CB – Dynamic content. Settings are seamless. No further changes are needed.

Thats all :)
