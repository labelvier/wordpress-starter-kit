<?php $content_data = get_query_var( 'content_data' );

if ($image = $content_data['image']) :
    $behaviour = 'cover' ?>
	<div class="cb-dynamic-content__image u-image-background-container cb-dynamic-content__image-behaviour--<?= $behaviour ?>">
		<?php if ( $image ) : ?>
			<?= wp_get_attachment_image_with_focal_point( $image, 'item') ?>
		<?php endif; ?>
    </div>
<?php endif; ?>


