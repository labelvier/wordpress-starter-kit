<?php
$content_selected = get_query_var( 'content_selected' )['value'];
$column_data = get_query_var( 'column_data' );
if ( $content_selected === 'wysiwyg' ):
	set_query_var( 'content_data', $column_data['wysiwyg'] );
	get_template_part( 'template-parts/blocks/dynamic-content-wysiwyg' );
elseif ( $content_selected === 'image' ):
    set_query_var( 'content_data', $column_data['image'] );
    get_template_part( 'template-parts/blocks/dynamic-content-image' );
elseif ( $content_selected === 'video' ):
    set_query_var( 'content_data', $column_data['video'] );
    get_template_part( 'template-parts/blocks/dynamic-content-video' );
endif;
