<?php $content_data = get_query_var('content_data');

if ($image = $content_data['image']) :
    $video_url = $content_data['video_url']; ?>
    <div class="cb-dynamic-content__video">
        <div class="o-content-container">
            <a href="<?= $video_url ?>" class="glightbox u-image-background-container" target="_blank">
                <i class="icon-play"></i>
                <?= wp_get_attachment_image_with_focal_point($image, 'item') ?>
            </a>
        </div>
    </div>

    <script type="text/javascript">
        window.addEventListener('load', function () {
            const lightbox = GLightbox();
        });
    </script>

<?php endif ?>
