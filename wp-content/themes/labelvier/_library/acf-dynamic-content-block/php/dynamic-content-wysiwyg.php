<?php $content_data = get_query_var( 'content_data' );

if ($wysiwyg = $content_data['wysiwyg']) : ?>
    <div class="wysiwyg-styling"><?= $wysiwyg ?></div>
<?php endif; ?>
