<?php
$column_selected = get_sub_field('column_select')['value'];
$column_1 = get_sub_field('column_1');
$column_1_placement = get_sub_field('column_1_placement');
$column_2 = get_sub_field('column_2');
$column_3 = get_sub_field('column_3');
?>
<section class="cb cb-dynamic-content
                cb-dynamic-content__background--<?= get_sub_field('background_color')['value'] ?>
                cb-dynamic-content__first-column--<?= $column_1['select']['value'] ?>
                <?php if ($column_selected === 'none'): echo 'cb-dynamic-content__placement--';
    echo $column_1_placement; endif; ?>
                ">
    <div class="cb-anchor" id="cb<?= get_row_index() ?>"></div>

    <div class="o-container">

        <?php if ($column_selected === 'none'): ?>
            <div class="o-container--text">
                <div class="o-layout">
                    <div class="o-layout__item u-size-12">
                        <?php
                        set_query_var('content_selected', $column_1['select']);
                        set_query_var('column_data', $column_1);
                        get_template_part('template-parts/blocks/dynamic-content-select');
                        ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($column_selected === '2'):
            $column_distribution = get_sub_field('2_column_distribution')['value']; ?>
            <div class="o-layout">
                <div class="o-layout__item u-size-12 <?php
                if ($column_distribution === '0') : echo 'u-size-6@tablet';
                elseif ($column_distribution === '1') : echo 'u-size-8@tablet';
                elseif ($column_distribution === '2') : echo 'u-size-4@tablet';
                elseif ($column_distribution === '3') : echo 'u-size-9@tablet';
                elseif ($column_distribution === '4') : echo 'u-size-3@tablet';
                endif; ?>">
                    <?php
                    set_query_var('content_selected', $column_1['select']);
                    set_query_var('column_data', $column_1);
                    get_template_part('template-parts/blocks/dynamic-content-select');
                    ?>
                </div>
                <div class="o-layout__item u-size-12 <?php
                if ($column_distribution === '0') : echo 'u-size-6@tablet';
                elseif ($column_distribution === '1') : echo 'u-size-4@tablet';
                elseif ($column_distribution === '2') : echo 'u-size-8@tablet';
                elseif ($column_distribution === '3') : echo 'u-size-3@tablet';
                elseif ($column_distribution === '4') : echo 'u-size-9@tablet';
                endif; ?>">
                    <?php
                    set_query_var('content_selected', $column_2['select']);
                    set_query_var('column_data', $column_2);
                    get_template_part('template-parts/blocks/dynamic-content-select');
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($column_selected === '3'):
            $column_distribution = get_sub_field('3_column_distribution')['value'] ?>
            <div class="o-layout">
                <div class="o-layout__item u-size-12 <?php
                if ($column_distribution === '0') : echo 'u-size-4@tablet';
                elseif ($column_distribution === '1') : echo 'u-size-6@tablet';
                else : echo 'u-size-3@tablet'; endif; ?>">
                    <?php
                    set_query_var('content_selected', $column_1['select']);
                    set_query_var('column_data', $column_1);
                    get_template_part('template-parts/blocks/dynamic-content-select');
                    ?>
                </div>
                <div class="o-layout__item u-size-12 <?php
                if ($column_distribution === '0') : echo 'u-size-4@tablet';
                elseif ($column_distribution === '2') : echo 'u-size-6@tablet';
                else : echo 'u-size-3@tablet'; endif; ?>">
                    <?php
                    set_query_var('content_selected', $column_2['select']);
                    set_query_var('column_data', $column_2);
                    get_template_part('template-parts/blocks/dynamic-content-select');
                    ?>
                </div>
                <div class="o-layout__item u-size-12 <?php
                if ($column_distribution === '0') : echo 'u-size-4@tablet';
                elseif ($column_distribution === '3') : echo 'u-size-6@tablet';
                else : echo 'u-size-3@tablet'; endif; ?>">
                    <?php
                    set_query_var('content_selected', $column_3['select']);
                    set_query_var('column_data', $column_3);
                    get_template_part('template-parts/blocks/dynamic-content-select');
                    ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
</section>
