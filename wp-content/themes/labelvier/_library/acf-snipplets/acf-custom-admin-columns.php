<?php


/**
 * Add custom admin column
 * Re-arrange columns (data after custom)
 * @param $columns
 * @return mixed
 */
function labelvier_add_acf_columns ( $columns ) {
	$date = $columns['date'];
	unset( $columns['date'] );
	$columns['custom_acf_field'] = __( 'Custom column' );
	$columns['date'] = $date;
	return $columns;
}
add_filter ( 'manage_cptname_posts_columns', 'labelvier_add_acf_columns' );

/**
 * Populate custom admin column
 * @param $column
 * @param $post_id
 */
function labelvier_fill_acf_column ( $column, $post_id ) {
	switch ( $column ) {
		case 'custom_acf_field':
			$acf_value = get_post_meta ( $post_id, 'custom_acf_field', true );
			echo "<a href='" . get_site_url() . "/wp-admin/post.php?post=" . $post_id . "&action=edit'>" . $acf_value . "</a>";
			break;
	}
}
add_action ( 'manage_cptname_posts_custom_column', 'labelvier_fill_acf_column', 10, 2 );

/**
 * Make custom admin column sortable
 * @param $columns
 * @return mixed
 */
function labelvier_sortable_acf_column( $columns ) {
	$columns['custom_acf_field'] = 'custom_acf_field';
	return $columns;
}
add_filter( 'manage_edit-cptname_sortable_columns', 'labelvier_sortable_acf_column' );
