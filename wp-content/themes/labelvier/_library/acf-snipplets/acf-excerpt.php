<?php

/**
 * Use an ACF field to fill the core-excerpt
 * @param $value
 * @param $post_id
 * @return mixed
 */
function acf_set_excerpt( $value, $post_id ) {
    if($value !== ''){
        wp_update_post([
            'ID' => $post_id,
            'post_excerpt' => $value,
        ]);
    }
    return $value;
}
add_filter('acf/update_value/key=field_618cc9b9e92bd', 'acf_set_excerpt', 10, 3);
