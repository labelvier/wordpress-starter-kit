<?php

/**
 * Save ACF fields used in frontend forms to revisions
 */
add_action('wp', 'force_revisions_save', 1);
function force_revisions_save() {
    global $post;
    if(empty($post->ID)) {
        return;
    }
    if (is_admin()) {
        // not needed in admin
        return;
    }
    $post_id = $post->ID;
    $post_type = get_post_type($post_id);
    if ($post_type != 'fieldtrip') {
        // only run on your post type
        return;
    }

    // add a hook that we want to copy the meta data to the revision
    add_action('_wp_put_post_revision', static function($revision_id) use ($post_id) {
        // add a filter when acf is updating to also copy the meta data to the revision
        add_action('acf/save_post', static function($updated_post_id) use ($post_id, $revision_id) {
            if($updated_post_id !== $post_id) {
                return;
            }
            // copy the updated post data to the revision
            acf_copy_metadata($post_id, $revision_id);
        },10, 1);
    });
}
