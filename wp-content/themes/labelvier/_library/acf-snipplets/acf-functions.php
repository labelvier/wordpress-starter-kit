<?php


/**
 * Set featured image when ACF image is added
 * Remove featured image when ACF image is removed
 * Add correct ACF field key
 * @param $value
 * @param $post_id
 * @return mixed
 */
function acf_set_featured_image_from_acf( $value, $post_id ) {
	if($value != ''){
		update_post_meta($post_id, '_thumbnail_id', $value);
	} else {
		delete_post_meta($post_id, '_thumbnail_id');
	}
	return $value;
}
add_filter('acf/update_value/key=field_600838b028008', 'acf_set_featured_image_from_acf', 10, 3);


/**
 * Set featured image from first ACF gallery image
 * Remove featured image when ACF images are removed
 * Add correct ACF field key
 * @param $value
 * @param $post_id
 * @return mixed
 */
function acf_set_featured_image_from_acf_gallery() {
	global $post;
	// Check if the post is new or existing
	if (!isset($post->ID)) {
		return;
	}
	$post_id = $post->ID;
	$has_thumbnail = get_the_post_thumbnail($post_id);
	// Check if gallery is present in post
	if (!$images = get_field('gallery', $post_id, false)) {
		return;
	}
	if ( $images ) {
		if (!$has_thumbnail && $image_id = $images[0]) {
			set_post_thumbnail( $post_id, $image_id );
		}
	} else {
		delete_post_thumbnail($post_id);
	}
}
add_action( 'save_post', 'acf_set_featured_image_from_acf_gallery' );



/**
 * Make ACF content field act as excerpt
 * use as: <div><?= acf_get_excerpt() ?></div>
 *
 * @return string
 */
function acf_get_excerpt(){
    $excerpt = get_field('content');
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 200);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt .= ' ...';
    return $excerpt;
}


/**
 * Move WP Post Content field to ACF tabbed field in place of placeholder-field (message
 */
function my_acf_admin_head() {
    ?>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function(){
                $('.acf-field-5ec26272c51b6 .acf-input').append( $('#postdivrich') );
            });
        })(jQuery);
    </script>
    <style type="text/css">
        .acf-field #wp-content-editor-tools {
            background: transparent;
            padding-top: 0;
        }
    </style>
    <?php
}
add_action('acf/input/admin_head', 'my_acf_admin_head');


/**
 * Customise flexible content overview
 */
function my_acf_fields_flexible_content_layout_title( $title, $field, $layout, $i ) {
	if ($column_select = get_sub_field( 'column_select' )['label']) {
		$title = 'Dynamic content: ';
		if ($column_select === 'Geen') {
			$title = esc_html__('Text: ', 'labelvier');
		} else {
			$title .= esc_html($column_select);
		}
	}
	if ($image = get_sub_field( 'image' )) {
		$title .= ' <span class="thumbnail" style="position: absolute;left:100px;top:-5px;"><img src="' . wp_get_attachment_image_url($image) . '" height="45px" /></span>';
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title/name=blocks', 'my_acf_fields_flexible_content_layout_title', 10, 4);
