<section>
    <h1>Image that works with the LabelVier Focal-point plugin</h1>

    <?php if ( $image = get_post_thumbnail_id() ) : ?>
        <?= wp_get_attachment_image_with_focal_point( $image, 'item') ?>
    <?php endif; ?>
</section>


<section>
    <h1>A link or button</h1>

    <?php if ($link = get_sub_field('link')): ?>
        <a class="button" href="<?= $link['url'] ?>" role="button"
           target="<?= $link['target'] ?: '_self' ?>"><?= $link['title'] ?>
        </a>
    <?php endif; ?>
</section>
