# ACF - Two Columns
Create a Two Column Content Block that can select existing Flexible Content Blocks.

1. Import ACF JSON
2. Create Flexible Content Block
3. Add templates

## 1. Import ACF JSON
Import the acf-Two-Column-Selector.json file into ACF. This will create an ACF Field Group named Two Column Selector.

## 2. Create Flexible Content Block
Go to the ACF Field Group 'Content Blocks' and add a new Layout (Label: Twee kolommen & Name: two-columns)

Add a Field
- Field Label: Links
- Field Name: left
- Field Type: Clone
- Fields: Two Column Selector - Kolom (group)
- Prefix Field Labels: Yes
- Prefix Field Names: Yes

Now duplicate this Field and rename Links to Rechts, and left to right

## 3. Add templates

Now the backend is ready. Lets add the templates. Copy the map and .php files from _library/blocks into the same locations of the theme template parts.
Check if they work by going to a page and add the Two Column Content block.
