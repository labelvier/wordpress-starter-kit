<?php
$left_column  = get_sub_field( 'left_column' );
$right_column = get_sub_field( 'right_column' );
?>

<div class="content-block__two-columns">
    <div class="o-layout@desktop">
		<?php if ( $left_column['select'] !== 'select' ) : ?>
            <div class="o-layout__item">
				<?php
				set_query_var( 'column_data', $left_column['text'] );
				get_template_part( 'two-columns/column-text' );
				?>
            </div>
		<?php endif; ?>

		<?php if ( $right_column['select'] !== 'select' ) : ?>
            <div class="o-layout__item">
				<?php
				set_query_var( 'column_data', $right_column['text'] );
				get_template_part( 'two-columns/column-text' );
				?>
            </div>
		<?php endif; ?>
    </div>
</div>
