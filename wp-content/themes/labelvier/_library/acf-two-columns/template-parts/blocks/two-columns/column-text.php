<?php
$column_data = get_query_var( 'column_data' );
?>
<div class="two-column-block__text">
	<?php if ( $text = $column_data['text'] ): ?>
		<?= $text ?>
	<?php endif; ?>
</div>
