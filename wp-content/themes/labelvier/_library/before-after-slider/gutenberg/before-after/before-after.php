<?php if ($cb_before_image = get_sub_field('before_image')) :
    // This is when this template is used as Content Block
    $before_image = $cb_before_image;
    $after_image = get_sub_field('after_image');
    echo '<script type="text/javascript" src="' . esc_url(get_template_directory_uri()) . '/template-parts/gutenberg/before-after/before-after.js"></script>';
elseif ($gut_before_image = get_field('before_image')) :
    // This is when this template is used as Gutenberg Block
    $before_image = $gut_before_image;
    $after_image = get_field('after_image');
endif; ?>

<div class="container">
    <div id="before-after-slider">
        <div id="before-image">
            <?php if (isset($before_image)) : ?>
                <?= wp_get_attachment_image_with_focal_point($before_image, 'item') ?>
            <?php else: ?>
                <img src="<?= esc_url(get_template_directory_uri()) ?>/template-parts/gutenberg/before-after/placeholder.jpg" alt="before">
            <?php endif; ?>
        </div>
        <div id="after-image">
            <?php if (isset($after_image)) : ?>
                <?= wp_get_attachment_image_with_focal_point($after_image, 'item') ?>
            <?php else: ?>
                <img src="<?= esc_url(get_template_directory_uri()) ?>/template-parts/gutenberg/before-after/placeholder.jpg" alt="After">
            <?php endif; ?>
        </div>
        <div id="resizer"></div>
    </div>
</div>
