<?php

/**
 * Before After ACF content block
 */
if( function_exists('acf_register_block_type') ):

    acf_register_block_type(array(
        'name' => 'before-after',
        'title' => 'Before after',
        'description' => 'Compare two images',
        'category' => 'common',
        'keywords' => array(
        ),
        'post_types' => array(
            0 => 'page',
            1 => 'project',
        ),
        'mode' => 'preview',
        'align' => '',
        'align_content' => NULL,
        'render_template' => 'template-parts/gutenberg/before-after/before-after.php',
        'render_callback' => '',
        'enqueue_style' => esc_url(get_template_directory_uri()) . '/template-parts/gutenberg/before-after/before-after.css',
        'enqueue_script' => esc_url(get_template_directory_uri()) . '/template-parts/gutenberg/before-after/before-after.js',
        'enqueue_assets' => '',
        'icon' => '',
        'supports' => array(
            'align' => true,
            'mode' => true,
            'multiple' => true,
            'jsx' => true,
            'align_content' => false,
            'anchor' => false,
        ),
    ));

endif;
