# Before after slider
Based on: https://codepen.io/pig3onkick3r/pen/YzqqWKY

1. Add the `gutenberg` folder to the `template-parts` folder.

2. Register block by copying the code from `gutenberg-ba-slider.php` to our `gutenberg.php` file.
 
Check it out and tweak styling
