<?php if ($video_url = get_field('video_url')): ?>
    <div class="event-details__video">
        <div class="video--container">
            <a href="<?= $video_url ?>"
               class="js-video-embed video-embed">
                <?php if ($image = get_field('video_still')) : ?>
                    <?= wp_get_attachment_image($image, 'item') ?>
                <?php endif; ?>
            </a>
        </div>
    </div>
<?php endif; ?>
