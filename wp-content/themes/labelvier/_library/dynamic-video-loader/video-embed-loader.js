document.addEventListener("DOMContentLoaded", init);

/**
 * Turns a URL to Youtube or Vimeo in a responsive Video Iframe
 */
let videos = null;

export default function init() {
    videos = document.querySelectorAll('.js-video-embed');
    [].forEach.call(videos, video => new VideoLoader(video));
}

/**
 * Loads the video on the poster click
 */
class VideoLoader {
    constructor($element) {
        this.$element = $element;
        if (!($element instanceof HTMLElement)) return;
        this.$otherVideos = this.getOtherVideos();
        this.isLoading = false;
        this.$element.addEventListener('click', this.renderIframe.bind(this));
    }

    /**
     * Get the Youtube-URL
     * @param {string} url
     */
    isYoutubeID(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        const match = url.match(regExp);
        if (match && match[2].length == 11) return match[2];
        return false;
    }

    /**
     * Get the Vimeo URL
     * @param {String} url
     */
    isVimeoID(url) {
        const regExp = /^.*vimeo.com\/(\d+)($|\/)/;
        const match = url.match(regExp);
        if (match[1]) return match[1];
        return false;
    }

    /**
     * Get the Video ID
     * @param {String} url
     */
    getVideoID(url) {
        let id = this.isYoutubeID(url);
        if (id) return {
            source: 'youtube',
            id: id
        };

        id = this.isVimeoID(url);
        if (id) return {
            source: 'vimeo',
            id: id
        };

        console.error('Could not extract ID from video URL')
        return false;
    }

    /**
     * Create an iframe from the video id
     *
     * @param {Object} match
     */
    createIframe(match) {
        if (match.source === 'youtube') {
            return this.createYoutubeIframe(match.id);
        }

        if (match.source === 'vimeo') {
            return this.createVimeoIframe(match.id);
        }

        return false;
    }

    /**
     * Create Youtube Iframe
     *
     * @param {string} id
     */
    createYoutubeIframe(id) {
        const iFrameContainer = document.createElement('div');
        iFrameContainer.setAttribute('class', 'video-embed__video-container');
        const iFrame = `<iframe allow="autoplay" allowfullscreen="" frameborder="0"  src="https://www.youtube-nocookie.com/embed/${id}?rel=0&showinfo=0&autoplay=1">`;
        iFrameContainer.innerHTML = iFrame;
        return iFrameContainer;
    }

    /**
     * Create Vimeo Iframe
     *
     * @param {string} id
     */
    createVimeoIframe(id) {
        const iFrameContainer = document.createElement('div');
        iFrameContainer.setAttribute('class', 'video-embed__video-container');
        const iFrame = `<iframe frameborder="0" allowfullscreen src="http://player.vimeo.com/video/${id}?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff&autoplay=1">`;
        iFrameContainer.innerHTML = iFrame;
        return iFrameContainer;
    }

    /**
     * Get the other videos on the page
     */
    getOtherVideos() {
        if (!videos) return [];
        return [].filter.call(videos, video => video != this.$element);
    }

    /**
     * Stop other iframes on play video
     */
    stopOtherIframes() {
        [].forEach.call(this.$otherVideos, video => {
            video.classList.remove('is-loading');
            video.classList.remove('is-playing');
            const iFrameContainer = video.querySelector('.c-card-video__container');
            if (!iFrameContainer) return;
            iFrameContainer.parentElement.removeChild(iFrameContainer);
        });
    }

    /**
     * Align video center of media block;
     */
    alignMediaBlockParent() {
        const mediaBlock = this.$element.closest('.block-media-text .o-layout--without-gutter');
        if (mediaBlock) mediaBlock.classList.add('o-layout--align-middle');
    }

    /**
     * Render the Iframe on click link video
     * @param {Event} event
     */
    renderIframe(event) {
        if (!this.isLoading) {
            this.isLoading = true;
            const target = event.currentTarget;
            const url = target.getAttribute('href');
            if (!url) return true;
            const match = this.getVideoID(url);
            if (!match) return true;
            const iFrame = this.createIframe(match);
            if (!iFrame) return true;

            this.stopOtherIframes();
            this.alignMediaBlockParent();

            target.classList.add('is-loading');
            target.appendChild(iFrame);
            iFrame.addEventListener('load', () => {
                target.classList.remove('is-loading');
                target.classList.add('is-playing');
                this.isLoading = false;
            }, true);
        }
        ;
        event.preventDefault();
        event.stopPropagation();
    }
}
