# FacetWP Mobile Flyout filter

An easy way to add a flyout filter function of your facetwp filters for tablet and below.


1. Add the following to the HTML above the filter parent container.

        <button class="flyout-filter--toggle button--solid u-show-below@tablet" onclick="toggleFlyoutFilter()">Filter</button>
        <div class="flyout-filter__overlay" onclick="toggleFlyoutFilter()"><!-- EMPTY --></div>
    
2. Add this class `flyout-filter` to the filter parent container.

3. Add a class or wrapper with class .flyout-filter to the filter container

4. Add this HTML in .flyout-filter

        <div role="button" class="flyout-filter--close u-show-below@tablet--flex" onclick="toggleFlyoutFilter()">Naar resultaat</div>

5. Add this script below the filter parent container

        <script>
            function toggleFlyoutFilter() {
                document.body.classList.toggle('flyout-filter--active');
            }
        </script>
    
6. Add the file _facetwp-flyout.scss to your scss folder
 
 Check it out and tweak styling
