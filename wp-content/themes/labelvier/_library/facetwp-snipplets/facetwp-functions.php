<?php

/**
 * Search filter and ordering
 */
// With a search facet is in use, the results are automatically ordered by relevancy. To disable this feature, add the following code:
add_filter( 'facetwp_use_search_relevancy', '__return_false' );



/**
 * Pre-fill (date) fields
 */
add_filter( 'facetwp_preload_url_vars', function ( $url_vars ) {
	if ( empty( $url_vars ) && is_post_type_archive('course_date') ) {
		$url_vars['course_date_start'] = array( date( 'Y-m-d', strtotime('-2 weeks') ) );
	}
	return $url_vars;
} );


/**
 * Sort by (start date)
 * Check if menu still works. If not use "Fix menu on custom taxonomy pages"
 */
add_action( 'pre_get_posts', 'my_change_sort_order');
function my_change_sort_order($query){
	if(is_post_type_archive('course_date') && $query->is_main_query()):
		//If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
		//Set the order ASC or DESC
		$query->set( 'meta_key', 'startdatum');
		$query->set( 'order', 'ASC' );
		//Set the orderby
		$query->set( 'orderby', 'meta_value' );
	endif;
}

/**
 * Scroll to top when using FacetWP pagination
 */
add_filter('facetwp_shortcode_html', function($html, $attr) {
	if ( isset( $attr['pager'] ) ) {
		$html .= '<script>
                document.addEventListener("DOMContentLoaded", function() {
                    document.addEventListener("facetwp-loaded", function() {
                        window.scrollTo(0, 0);
                    });
                });
            </script>';
	}
	return $html;
}, 10, 2);
