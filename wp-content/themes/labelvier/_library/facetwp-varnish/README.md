# Facetwp Varnish Filter
This combination of javascript hook and PHP file changes the facetwp_refresh call to a GET call which get indexed by varnish

## Installation
1) Move `facetwp-varnish.php` to the `inc` folder and add an include in `functions.php`
2) Move `facetwp-varnish.js` to `src/js/components` and add an include in `budle.js`

