/**
 * Override facetwp_refresh to GET method to use varnish
 */
if(typeof jQuery !== 'undefined') {
    jQuery(document).ready(function ($) {
        if (typeof window.FWP !== 'undefined') {
            window.FWP.hooks.addFilter('facetwp/ajax_settings', function (settings) {
                if (settings.data && settings.data.action === "facetwp_refresh") {
                    settings.type = "GET";
                }
                return settings;
            });
        }
    });
}
