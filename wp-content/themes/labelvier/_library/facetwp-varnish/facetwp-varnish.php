<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 30-09-2020
 * Time: 12:54
 */
// Revert varnish trick facetwp_refresh back to POST
if(isset($_GET['action']) && $_GET['action'] === 'facetwp_refresh') {
	$_POST = $_GET;
}
