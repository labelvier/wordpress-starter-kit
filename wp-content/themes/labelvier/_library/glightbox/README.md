# GLightbox

A touchable Pure Javascript lightbox with mobile and video support.

GLightbox is "ready for use" in the starter-kit and now made available through node_modules.

https://biati-digital.github.io/glightbox/

## How to implement GLightbox

1. Import GLightbox in `src/js/bundle.js`
     
    `import GLightbox from 'glightbox';`
    
    `window.GLightbox = GLightbox;`

2. Import default and custom GLightbox styling in `src/scss/style.scss`

    `@import "../../node_modules/glightbox/dist/css/glightbox";`
    
    `@import "g-plugins/glightbox";`

3. Use the HTML in the example template here in `_library/glightbox/glightbox-html.php` and make it yours.
