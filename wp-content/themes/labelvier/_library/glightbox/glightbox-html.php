<div class="gallery">
	<a href="<?= wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ) ?>"
	   data-gallery="gallery1" class="glightbox gallery__featured-image u-image-background-container" target="_blank">
		<?= wp_get_attachment_image_with_focal_point( get_post_thumbnail_id(), 'large' ) ?>
	</a>
	<?php if ( $images = get_field('images') ): ?>
		<ul>
			<?php foreach ( $images as $image ): ?>
				<li>
					<a href="<?= wp_get_attachment_image_url( $image, 'large' ) ?>"
					   data-gallery="gallery1" class="glightbox gallery__image" target="_blank">
						<?= wp_get_attachment_image( $image, 'thumbnail' ) ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>
<script type="text/javascript">
    window.addEventListener('load', function () {
        const lightbox = GLightbox({
            touchNavigation: true,
            loop: true
        });
    });
</script>




<?php if ( $youtube_link = get_field('youtube_link') ):
	$youTubeParts = explode( 'v=', $youtube_link );
	$youtubeId    = $youTubeParts[1]; ?>
	<script>
        let videoGallery;
        window.addEventListener('load', function () {
            videoGallery = GLightbox({
                elements: [
                    {
                        'href': '//www.youtube-nocookie.com/embed/<?= $youtubeId ?>',
                        'type': 'video',
                        'source': 'youtube', //vimeo, youtube or local
                        'width': 900,
                    },
                ]
            });
        });
	</script>
	<div class="gallery__video">
		<h3 class="gallery__video-header">Watch a video</h3>
		<span onclick="videoGallery.open();" class="gallery__video--thumb"></span>
		<style>
            .gallery__video--thumb {
	            display: block;
	            width: 100%;
	            height: 30rem;
                background-image: url('//img.youtube.com/vi/<?= $youtubeId ?>/hqdefault.jpg');
            }
		</style>
	</div>
<?php endif; ?>
