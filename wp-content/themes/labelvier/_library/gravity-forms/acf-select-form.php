<?php

/**
 * Gets all available forms in puts them in the select box
 *
 * @param array $field
 * @return array
 */
function subscribe_field_choices($field) {
	// reset choices
	$field['choices'] = array();
	//fill choices from gravity forms
	$forms = GFAPI::get_forms();
	foreach($forms as $form) {
		$field['choices'][$form['id']] = $form['title'];
	}
	//return field again
	return $field;
}
add_filter('admin_init', function() {
	if (!is_plugin_active('gravityforms/gravityforms.php')) {
		return;
	}
	add_filter('acf/load_field/name=select_form', 'subscribe_field_choices');
});
