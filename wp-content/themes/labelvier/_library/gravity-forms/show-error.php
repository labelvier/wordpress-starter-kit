<?php
/**
 * Jump to the first error if there is any after clicking send
 *
 * @param $form
 * @return mixed
 */
if (function_exists('gravity_form')) {
	function gf_scroll_to_first_error_focus( $form ) {
		?>
        <script type="text/javascript">
            if( window['jQuery'] ) {
                ( function( $ ) {
                    $( document ).bind( 'gform_post_render', function() {
                        var $firstError = $( 'li.gfield.gfield_error:first' );
                        if( $firstError.length > 0 ) {
                            $firstError.find( 'input, select, textarea' ).eq( 0 ).focus();
                            document.body.scrollTop = $firstError.offset().top;
                        }
                    } );
                } )( jQuery );
            }
        </script>
		<?php
		return $form;
	}
	add_action( 'gform_pre_render', 'gf_scroll_to_first_error_focus', 10, 1 );
}
