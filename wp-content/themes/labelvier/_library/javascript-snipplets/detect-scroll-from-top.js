document.addEventListener("scroll", (e) => {
    let scrolled = document.scrollingElement.scrollTop;
    let position = document.body.offsetTop;

    if (scrolled > position + 100) {
        document.body.classList.add('header--fixed');
    } else {
        document.body.classList.remove('header--fixed');
    }
});
