<?php
/**
 * Check string for http(s)
 *
 * if $returnWithHttp = true, force https
 * if $returnWithHttp = false, remove http(s)
 * @param string $string = url as string, with or without http(s)
 * @param bool $returnWithHttps = boolean
 */
function string_to_url_checker( string $string, bool $returnWithHttps = true) {
	if ($returnWithHttps) {
		if ( strpos( $string, "http://" ) !== 0 && strpos( $string, "https://" ) !== 0 ) {
			return 'https://' . $string;
		}
		if ( strpos( $string, "http://" ) === 0 ) {
			return str_replace('http://', 'https://', $string);
		}
		return $string;
	}
	if (!$returnWithHttps) {
		$disallowed = array('http://', 'https://');
		foreach($disallowed as $d) {
			if(strpos($string, $d) === 0) {
				return str_replace($d, '', $string);
			}
		}
		return $string;
	}
}
