<?php if (get_field('show_video')) : ?>
    <?php if ($video = get_field('video')) : ?>
        <video id="header-video" class="u-show-above@mobile" muted playsinline loop preload autoplay></video>
        <script>
            const headerVideo = document.getElementById('header-video');
            if (window.innerWidth > 576) {
                let source = document.createElement('source');
                source.setAttribute('src', '<?= $video ?>');
                source.setAttribute('type', 'video/mp4');
                headerVideo.appendChild(source);
            }
        </script>
    <?php endif; ?>
<?php endif; ?>
