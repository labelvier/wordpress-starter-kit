<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 22-09-2021
 * Time: 16:35
 */

/**
 * Get available translations for current page
 *
 * @return mixed
 */
function multilingualpress_get_translations()
{
	$args = \Inpsyde\MultilingualPress\Framework\Api\TranslationSearchArgs::forContext(new \Inpsyde\MultilingualPress\Framework\WordpressContext())
	                                                                      ->forSiteId(get_current_blog_id())
	                                                                      ->includeBase();

	$translations = \Inpsyde\MultilingualPress\resolve(
		\Inpsyde\MultilingualPress\Framework\Api\Translations::class
	)->searchTranslations($args);

	return $translations;
}

/**
 * Add global window.language object to tell js the available languages
 */
function labelvier_language_switcher() {
	$translations = multilingualpress_get_translations();
	$languages = [];
	foreach ($translations as $translation) {
		$language = $translation->language();
		$http_code = $language->bcp47tag();
		$url = $translation->remoteUrl();
		$languages[] = [
			"http_code" => strtolower($http_code),
			"url" => $url,
			"is_current" => $translation->remoteSiteId() === get_current_blog_id()
		];

	}
	wp_localize_script('labelvier-head-js', 'language', [
		'available_languages' => $languages,
		'current_blog' => get_current_blog_id(),
		'current_locale' => get_locale()
	]);
}
add_action( 'wp_enqueue_scripts', 'labelvier_language_switcher', 11 );

/**
 * Add the redirect_lang param to make sure the checker stays at the right language.
 *
 * @param $atts
 * @param $item
 * @param $args
 * @param $depth
 *
 * @return mixed
 */
function labelvier_add_language_switcher_arguments($atts, $item, $args, $depth ) {
	if($args->menu->slug === "language-switcher") {
		$atts["href"] .= '?redirect_lang=true';
	}
	return $atts;
}
add_filter('nav_menu_link_attributes', 'labelvier_add_language_switcher_arguments', 10, 4);
