let current_language = window.localStorage.getItem('active_language') || (window.navigator.userLanguage || window.navigator.language);
current_language = current_language.substring(0, 2);

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
if(urlParams.get('redirect_lang')) {
    const current_locale = window.language.current_locale.substring(0, 2);
    // save the active lang to stay at this language // only 1 week in safari
    window.localStorage.setItem('active_language', current_locale);
} else {
    // window.language is the global multi language object send from php
    // check if we need to redirect
    for (const available_language of window.language.available_languages) {
        if(current_language === available_language.http_code.substring(0, 2) && available_language.url && !available_language.is_current) {
            // redirect!
            const url = new URL(available_language.url);
            url.searchParams.append('redirect_lang', 'true');
            window.location = url.toString();
        }
    }
}
