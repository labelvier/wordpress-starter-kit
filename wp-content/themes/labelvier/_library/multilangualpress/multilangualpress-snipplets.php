<h1>Language switcher menu</h1>

<h2>Add this part to the html</h2>
<?php wp_nav_menu( array(
	'container'      => false,
	'fallback_cb'    => false,
	'theme_location' => 'language-menu',
	'items_wrap'     => labelvier_language_menu_wrapper(),
) ); ?>


<h2>Add this part to functions.php</h2>
<?php

/**
 * Language menu wrapper
 * Creates a dropdown Toggle around the desktop language menu
 * @return string
 */
use function Inpsyde\MultilingualPress\currentSiteLocale;
function labelvier_language_menu_wrapper() {
	if ( !function_exists('Inpsyde\MultilingualPress\currentSiteLocale') ) {
		$current_language = 'nl';
	} else {
		$current_language = currentSiteLocale();
		if ( empty($current_language) ) {
			return false;
		}
		$current_language = substr($current_language, 0, 2);
	}
	if ( $current_language === 'nl') {
		$show_current_language = 'Nederlands';
	} elseif ( $current_language === 'be') {
		$show_current_language = 'Belgisch';
	} elseif ( $current_language === 'fr') {
		$show_current_language = 'Français';
	} elseif ( $current_language === 'en') {
		$show_current_language = 'English';
	}

	// Add class to menu item: .lang-switch__lang--en
	return '<div class="lang-switch"><div class="lang-switch__inner">
    <span class="lang-switch__current-lang lang-switch__lang--' . $current_language . '">' . $show_current_language . '</span>
      <ul class="lang-switch__menu">%3$s
      </ul>
  	</div></div>';
}

?>

<h2>Fill the menu in WordPress with custom links to the other language site(s)</h2>
<p>The menu name will be the name of the current lang in the menu</p>
