<?php if (reading_time() > 0) : ?>
<time>
    Leestijd <?php echo esc_html( '' . reading_time() ); ?>
</time>
<?php endif; ?>