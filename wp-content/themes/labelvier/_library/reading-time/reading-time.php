<?php
/**
 * Labelvier reading time calculation
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package labelvier
 */
function reading_time() {

	$id          = get_the_ID();
	$content     = get_the_content();
	$word_count  = str_word_count( strip_tags( $content ) );
	$readingtime = $word_count / 275 * 60; // 275 words per minute -> In seconds.

	$dom = new domDocument();
    if($content){
        $dom->loadHTML( $content );
    }
    $dom->preserveWhiteSpace = false;
	$images = $dom->getElementsByTagName( 'img' );

	// $extra_media_seconds = '';
	if ( $images ) {
		$i = 0;
		foreach ( $images as $image ) {
			if ( 10 > $i ) {
				// $extra_media_seconds .= ' ' . ( 12 - $i );
				$readingtime += ( 12 - $i );
			} elseif ( 10 <= $i ) {
				// $extra_media_seconds .= ' ' . ( 3 );
				$readingtime += ( 3 );
			}
			$i++;
		}
		$readingtime = $readingtime / 60; // Total reading time in minutes.
	}

	$readingtime = ceil($readingtime);

	 if ( 1 == $readingtime ) {
	 	$timer = ' minuut';
	 } else {
	 	$timer = ' minuten';
	 }

	$totalreadingtime = $readingtime . $timer;

	return $totalreadingtime;
}
