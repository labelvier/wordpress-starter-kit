# Related post
Function which can be used to get any related posts or other CPT (filter on any taxonomy)

## Installation
Move `related-posts.php` to the `inc` folder and add an include in `functions.php`

## Usage
See `example.php`
