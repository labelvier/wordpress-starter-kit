<?php
$related_posts = labelvier_get_related_posts();

if ($related_posts->post_count) :
    foreach ($related_posts->posts as $post) :
        setup_postdata($post);

        the_title();

    endforeach;
    wp_reset_postdata();
endif;
