<?php

/**
 * Returns related posts based on taxonomies
 *
 * @param string $post_type The post type for the related posts
 * @param array|string $taxonomies Array of taxonomies where you want to query on
 * @param bool|WP_Post $post Reference Post, defaults to global post object if empty
 * @param int $post_count how many posts you want to return
 * @param string $taxonomy_match how to match multiple taxonomies
 *
 * @return WP_Query
 */
function labelvier_get_related_posts($post_type = 'post', $taxonomies = ['post_tag', 'category'], $post = false, $post_count = 2, $taxonomy_match = 'OR') {
	if($post === false) {
		global $post;
	}

	//setup tax query, we want a match with ANY of the taxonomies (for default)
	$tax_query = ['relation' => $taxonomy_match];

	if(is_string($taxonomies)) {
		$taxonomies = [$taxonomies];
	}
	foreach($taxonomies as $taxonomy) {
		$tax_query[] = [
			'taxonomy' => $taxonomy, // Taxonomy id
			'field'    => 'id',
			'terms'    => wp_get_object_terms( $post->ID, $taxonomy, [ 'fields' => 'ids' ] ),
			'operator' => 'IN'
		];
	}

	//check for related posts
	$args          = array(
		'post_count'     => $post_count,
		'posts_per_page' => $post_count,
		'max_num_pages'  => 1,
		'post_type'      => $post_type,
		'post_status'    => 'publish',
		'post__not_in'   => [ $post->ID ],
		'tax_query'      => $tax_query
	);

	//return the related_posts
	return new WP_Query( $args );
}
