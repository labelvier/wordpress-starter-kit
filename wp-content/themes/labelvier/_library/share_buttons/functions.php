<?php

/**
 * Sharing
 * https://crunchify.com/how-to-create-social-sharing-button-without-any-plugin-and-script-loading-wordpress-speed-optimization-goal/
 */
function crunchify_social_sharing_buttons() {
	global $post;
	if(is_singular() || is_home()){
		// Get current page URL
		$crunchifyURL = urlencode(get_permalink());

		// Get current page title
		$crunchifyTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');

		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL  = 'https://twitter.com/intent/tweet?text=' . $crunchifyTitle . '&amp;url=' . $crunchifyURL . '&amp;via=' . rawurlencode(get_bloginfo( 'name' ));
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$bufferURL = 'https://bufferapp.com/add?url='.$crunchifyURL.'&amp;text='.$crunchifyTitle;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url=' . $crunchifyURL . '&amp;title=' . $crunchifyTitle;
		$whatsappURL = 'whatsapp://send?text=' . $crunchifyTitle . ' ' . $crunchifyURL;
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;

		// Add sharing button at the end of page/page content
		$content = '<div class="crunchify">';
		$content .= '<h4 class="crunchify-header">' . esc_html__( 'Share this page:', 'labelvier' ) . '</h4>';
		$content .= '<ul>';
		$content .= '<li><a class="crunchify-link crunchify-twitter" href="' . $twitterURL . '" target="_blank"><i class="icon-twitter"></i><span>Twitter</span></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-facebook" href="' . $facebookURL . '" target="_blank"><i class="icon-facebook"></i><span>Facebook</span></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-linkedin" href="' . $linkedInURL . '" target="_blank"><i class="icon-linkedin"></i><span>LinkedIn</span></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><i class="icon-pinterest"></i><span>Pin It</span></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-whatsapp" href="' . $whatsappURL . '" target="_blank"><i class="icon-whatsapp"></i><span>WhatsApp</span></a></li>';
//        $content .= '<li><a class="crunchify-link crunchify-googleplus" href="'.$googleURL.'" target="_blank"><i class="icon-google-plus"></i><span>Google+</span></a></li>';
		$content .= '</ul></div>';

		return $content;
	} else {
		// if not a post/page then don't include sharing button
		return $content;
	}
}
