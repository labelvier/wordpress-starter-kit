# SwiperJs

Swiperjs is "ready for use" in the starter-kit and now made available through node_modules.

## How to implement swiperjs

1. Import Swiper in `src/js/bundle.js`
   
   `import Swiper, { Navigation, Pagination } from 'swiper';`
   
   `window.Swiper = Swiper.use([Navigation, Pagination]);`

    Features like navigation and pagination are modules that can be used by importing them.
    
    Other modules are 
      A11y,
      Autoplay,
      Controller,
      EffectCoverflow,
      EffectCube,
      EffectFade,
      EffectFlip,
      HashNavigation,
      History,
      Keyboard,
      Lazy,
      Mousewheel,
      Parallax,
      Scrollbar,
      Thumbs,
      Virtual,
      Zoom
      
      This way you will only include the parts you want to use.
      
      In depth info can be found here https://swiperjs.com/api/

2. Import default and custom swiper styling in `src/scss/style.scss`

    `@import "../../node_modules/swiper/swiper-bundle";`

    `@import "g-plugins/swiper";`

3. Use the HTML in the example template here in `_library/swiperjs/swiper-html.php` and make it yours.
