<div class="swiper-container swiper<?= get_row_index() ?>-container swiper">
    <div class="swiper-wrapper">
        <?php $posts = get_sub_field('slider');
        if ($posts):
            foreach ($posts as $post): ?>
                <div class="swiper-slide">
                    <?php if ($image = $post['image']) : ?>
                        <?= wp_get_attachment_image_with_focal_point($image, 'banner') ?>
                    <?php endif; ?>
                </div>
            <?php endforeach;
            wp_reset_postdata();
        endif; ?>
    </div>
    <div class="swiper-button swiper-button-prev swiper<?= get_row_index() ?>-button-prev"></div>
    <div class="swiper-button swiper-button-next swiper<?= get_row_index() ?>-button-next"></div>
    <div class="swiper-pagination swiper<?= get_row_index() ?>-pagination"></div>
</div>


<script>
    // add intersection observer to load slider when in viewport
    const swiper<?= get_row_index() ?>Observer = new IntersectionObserver(function (entries, observer) {
        if (entries[0].isIntersecting) {
            observer.unobserve(entries[0].target);
            const swiper<?= get_row_index() ?> = new Swiper('.swiper<?= get_row_index() ?>-container', {
                direction: 'horizontal',
                loop: true,
                slidesPerView: 1,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper<?= get_row_index() ?>-pagination',
                    type: 'bullets',
                    clickable: true
                },
                navigation: {
                    nextEl: '.swiper<?= get_row_index() ?>-button-next',
                    prevEl: '.swiper<?= get_row_index() ?>-button-prev',
                },
            });
            swiper<?= get_row_index() ?>Observer.unobserve(entries[0].target);
        }
    }, {rootMargin: '0px 0px 0px 0px'});
    swiper<?= get_row_index() ?>Observer.observe(document.querySelector('.swiper<?= get_row_index() ?>-container'));
</script>
