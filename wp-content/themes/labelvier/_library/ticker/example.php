<?php
/**
 * <div class="ticker__container" data-tickerMode="single" data-tickerSpeed="12" data-tickerDirection="ltr">
 *     // UL and LI can be DIVs as well
 *     <ul class="ticker__reel">
 *         <li>Item 1</li>
 *         <li>Item 2</li>
 *         <li>Item 3</li>
 *         <li>Item 4</li>
 *     </ul>
 * </div>
 */

// Default the ticker will activate the moment the USPs do not fit in the viewport
// If you want to activate the ticker earlier, you can set forcePlay to true
?>
<div class="ticker__container" data-tickerMode="push" data-tickerSpeed="12" data-tickerDirection="rtl" data-forcePlay="true">
    <ul class="ticker__reel">
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 2</li>
    </ul>
</div>
<div class="ticker__container" data-tickerMode="single" data-tickerSpeed="12" data-tickerDirection="rtl">
    <div class="ticker__reel">
        <div>Item 1</div>
        <div>Item 2</div>
        <div>Item 2</div>
    </div>
</div>
<div class="ticker__container" data-tickerMode="continuous" data-tickerSpeed="12" data-tickerDirection="ltr">
    <ul class="ticker__reel">
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 2</li>
    </ul>
</div>
