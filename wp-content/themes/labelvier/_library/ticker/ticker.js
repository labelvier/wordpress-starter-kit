/**
 * <div class="ticker__container" data-tickerMode="single" data-tickerSpeed="12" data-tickerDirection="ltr">
 *     // UL and LI can be DIVs as well
 *     <ul class="ticker__reel">
 *         <li>Item 1</li>
 *         <li>Item 2</li>
 *         <li>Item 3</li>
 *         <li>Item 4</li>
 *     </ul>
 * </div>
 */

document.addEventListener("DOMContentLoaded", function () {
    ticker();
});

/**
 * Ticker
 *
 */
ticker = function () {
    if (document.querySelector(".ticker__container")) {
        document.querySelectorAll(".ticker__container").forEach(function (tickerContainer) {

            // execute when the ticker is visible
            const observer = new IntersectionObserver(function (entries) {
                    entries.forEach(function (entry) {
                        if (entry.isIntersecting) {
                            tickerContainer.classList.add("ticker__container--active");
                            tickerContainer.classList.remove("ticker__container--inactive");
                        } else {
                            tickerContainer.classList.add("ticker__container--inactive");
                            tickerContainer.classList.remove("ticker__container--active");
                        }
                    });
                }
                , {threshold: [0, 1]});
            observer.observe(tickerContainer);

            // get data-tickerMode from tickerContainer
            // single: a pause is added after every loop
            // continuous: all items loop all the time
            // push: only one item is visible at a time
            const tickerMode = tickerContainer.getAttribute("data-tickerMode") || "single"; // single | continuous | push

            // get data-tickerSpeed="speed" from the ticker container with fallback
            const tickerSpeed = tickerContainer.getAttribute("data-tickerSpeed") || 30; // 1 - 99
            // get data-tickerDirection="direction" from the ticker container with fallback
            const tickerDirection = tickerContainer.getAttribute("data-tickerDirection") || 'ltr'; // ltr | rtl
            // get data-tickerStart
            const forcePlay = tickerContainer.getAttribute("data-forcePlay") || 'false'; // false | true

            // Add some basic styling to the ticker container
            tickerContainer.style.display = "flex";
            tickerContainer.style.opacity = 0;
            tickerContainer.style.overflow = "hidden";
            if (tickerDirection === 'rtl') {
                tickerContainer.style.justifyContent = "flex-end";
            } else {
                tickerContainer.style.justifyContent = "flex-start";
            }

            // tickerContainer visible after half a second
            setTimeout(function () {
                tickerContainer.style.transition = 'opacity 0.2s ease';
                tickerContainer.style.opacity = 1;
            }, 500);

            // Add some basic styling to tickerReel
            const tickerReel = tickerContainer.querySelector(".ticker__reel");
            const tickerReelWidth = tickerReel.offsetWidth;
            tickerReel.style.display = "flex";
            tickerReel.style.width = "100%";


            const tickerContainerWidth = tickerContainer.offsetWidth;

            let startTicker = function () {
                const wrapper = document.createElement("div");
                wrapper.classList.add("ticker__reel-scroller");
                tickerReel.parentNode.insertBefore(wrapper, tickerReel);
                wrapper.appendChild(tickerReel);
                wrapper.style.display = "flex";
                const tickerReelScroller = tickerContainer.querySelector(".ticker__reel-scroller");

                // Mode: push requires different calculations of width
                if (tickerMode === 'push') {
                    // Find the direct children of the tickerReel and set tickerContainerWidth
                    const tickerReelChildren = tickerReel.children;
                    for (let i = 0; i < tickerReelChildren.length; i++) {
                        tickerReelChildren[i].style.width = tickerContainerWidth + "px";
                    }
                    tickerReel.style.width = tickerReel.offsetWidth + 'px';
                } else {
                    tickerReel.style.width = tickerReel.scrollWidth + 'px';
                }

                // If tickerMode is push, show every item on its own with a pause and run infinite
                if (tickerMode === "push") {
                    tickerReelScroller.style.transition = "transform 1s ease";
                    const tickerReelClone = tickerReel.cloneNode(true);
                    tickerReelClone.classList.add("ticker__reel-clone");

                    if (tickerDirection === 'rtl') {
                        tickerReelScroller.prepend(tickerReelClone);
                    } else {
                        tickerReelScroller.append(tickerReelClone);
                    }

                    const tickerReelChildren = tickerReel.children;
                    const tickerReelChildrenCount = tickerReelChildren.length;

                    let tickerSpeed = 4000;
                    let tickerReelPosition = 0;
                    let currentItem = 1;
                    setInterval(function () {
                        currentItem++;
                        if (currentItem > tickerReelChildrenCount) {
                            currentItem = 1;
                            setTimeout(function () {
                                tickerReelPosition = 0;
                                tickerReelScroller.style.transition = "none";
                                tickerReelScroller.style.transform = "translateX(-" + tickerReelPosition + "px)";
                            }, 1000);
                        }
                        if (tickerContainer.classList.contains("ticker__container--inactive")) {
                            tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                        } else {
                            tickerReelScroller.style.transition = "transform 1s ease";
                            tickerReelPosition += tickerContainerWidth;
                        }
                        tickerReelScroller.style.transform = "translateX(-" + tickerReelPosition + "px)";
                    }, tickerSpeed);
                } else if (tickerMode === "continuous") {
                    // If tickerMode is continuous, duplicate the tickerReel and run infinite
                    const tickerReelClone = tickerReel.cloneNode(true);
                    tickerReelClone.classList.add("ticker__reel-clone");

                    if (tickerDirection === 'rtl') {
                        tickerReelScroller.prepend(tickerReelClone);
                    } else {
                        tickerReelScroller.append(tickerReelClone);
                    }
                    // Show lazy images in cloned tickerReel
                    const lazyImages = tickerReelClone.querySelectorAll("img.js-lazy");
                    lazyImages.forEach((image) => {
                        image.src = image.dataset.src;
                        image.classList.remove("anim-fadein");
                    });

                    // Set animation for continuous ticker
                    let tickerReelPosition = 0;
                    if (tickerDirection === 'rtl') {
                        setInterval(function () {
                            if (tickerContainer.classList.contains("ticker__container--inactive")) {
                                tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                            } else {
                                tickerReelPosition++;
                            }
                            if (tickerReelClone.getBoundingClientRect().right >= tickerContainer.getBoundingClientRect().right) {
                                tickerReelPosition = 0;
                            }
                            tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                        }, tickerSpeed);
                    } else {
                        setInterval(function () {
                            if (tickerContainer.classList.contains("ticker__container--inactive")) {
                                tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                            } else {
                                tickerReelPosition--;
                            }
                            if (tickerReelClone.getBoundingClientRect().left <= tickerContainer.getBoundingClientRect().left) {
                                tickerReelPosition = 0;
                            }
                            tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                        }, tickerSpeed);
                    }
                } else {
                    // If tickerMode is single, run infinite with a space between runs
                    const tickerReelSpacer = document.createElement("div");
                    tickerReelSpacer.style.width = tickerContainerWidth + "px";

                    if (tickerDirection === 'rtl') {
                        tickerReelScroller.append(tickerReelSpacer);
                    } else {
                        tickerReelScroller.prepend(tickerReelSpacer);
                    }

                    // Set animation for ticker with spacer
                    let tickerReelPosition = 0;
                    if (tickerDirection === 'rtl') {
                        setInterval(function () {
                            if (tickerContainer.classList.contains("ticker__container--inactive")) {
                                tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                            } else {
                                tickerReelPosition++;
                            }

                            if (tickerReel.getBoundingClientRect().left >= tickerContainer.getBoundingClientRect().right) {
                                tickerReelPosition = 0;
                            }
                            tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                        }, tickerSpeed);
                    } else {
                        setInterval(function () {
                            if (tickerContainer.classList.contains("ticker__container--inactive")) {
                                tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                            } else {
                                tickerReelPosition--;
                            }

                            if (tickerReel.getBoundingClientRect().right <= tickerContainer.getBoundingClientRect().left) {
                                tickerReelPosition = 0;
                            }
                            tickerReelScroller.style.transform = "translateX(" + tickerReelPosition + "px)";
                        }, tickerSpeed);
                    }
                }
            }

            let runTicker = function () {
                if (forcePlay === 'true') {
                    startTicker();
                } else if (tickerReelWidth >= tickerContainerWidth) {
                    startTicker();
                }
            }
            runTicker();

            // Run ticker on resize does nog work yet with current code
            // let resizeTimer;
            // window.addEventListener("resize", function () {
            //     clearTimeout(resizeTimer);
            //     resizeTimer = setTimeout(function () {
            //         runTicker();
            //     }, 250);
            // });
        });
    }
}
