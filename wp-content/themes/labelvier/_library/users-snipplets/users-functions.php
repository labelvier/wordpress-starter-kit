<?php

/**
 * Allow administrators to edit the Privacy Policy page
 * this is restricted by default for superusers only
 */
function custom_manage_privacy_options($caps, $cap, $user_id, $args) {
	if (!is_user_logged_in()) return $caps;
	$user_meta = get_userdata($user_id);
	if (array_intersect(['administrator'], $user_meta->roles)) {
		if ('manage_privacy_options' === $cap) {
			$manage_name = is_multisite() ? 'manage_network' : 'manage_options';
			$caps = array_diff($caps, [ $manage_name ]);
		}
	}
	return $caps;
}
if (is_user_logged_in()){
	add_action('map_meta_cap', 'custom_manage_privacy_options', 1, 4);
}


/**
 * Enable unfiltered_html capability for Editors.
 *
 * @param array $caps The user's capabilities.
 * @param string $cap Capability name.
 * @param int $user_id The user ID.
 * @return array $caps The user's capabilities, with 'unfiltered_html' potentially added.
 */
function km_add_unfiltered_html_capability_to_editors($caps, $cap, $user_id) {
	if ('unfiltered_html' === $cap && user_can($user_id, 'administrator')) {
		$caps = array('unfiltered_html');
	}
	return $caps;
}
add_filter('map_meta_cap', 'km_add_unfiltered_html_capability_to_editors', 1, 3);
