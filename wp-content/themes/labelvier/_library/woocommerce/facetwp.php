<?php if ( function_exists( 'FWP' ) ) :
    // Revert varnish trick facetwp_refresh back to POST
    if(isset($_GET['action']) && $_GET['action'] === 'facetwp_refresh') {
        $_POST = $_GET;
    }

    function woocommerce_result_count(): void {
        echo '<div class="woocommerce-result-count">';
        $counts = facetwp_display( 'counts' );
        echo 'Er worden hier ' . $counts . ' producten getoond';
        echo '</div>';
    }

    function woocommerce_pagination(): void {
        echo facetwp_display( 'pager' );
    }

    function labelvier_add_facetwp_js(): void {
        if ( ! function_exists('is_shop' ) ) {
            return;
        }
        if ( is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy() || is_product() ) {
            wp_enqueue_script( 'labelvier-facetwp', get_template_directory_uri() . '/dist/js/facetwp.js', [ 'jquery' ], false, true );
        }
    }
    add_action( 'wp', 'labelvier_add_facetwp_js' );
endif;
