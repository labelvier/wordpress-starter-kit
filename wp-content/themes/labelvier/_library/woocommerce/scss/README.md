# 1. Add g-plugins/woocommerce folder to theme/src/scss/g-plugins

# 2. Add custom woocommerce style support with wp-takeoff in terminal

`
wp-takeoff woocommerce add
`

# 3. Add @import rules to style.scss

`
@import "g-plugins/woocommerce/default";
@import "g-plugins/woocommerce/custom";
`

# 4. Dont forget to add styling for facetwp-flyout (also in the _library folder)
