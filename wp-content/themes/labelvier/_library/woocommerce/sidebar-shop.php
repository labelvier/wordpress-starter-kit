<?php
/**
 * The sidebar containing the shop widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LabelVier
 */
if(!function_exists('is_shop')) {
    return;
}
if ( ! is_shop() && ! is_product_category() && ! is_product_tag() && ! is_product_taxonomy() ) {
    return;
} ?>

<aside class="widget-area">
    <span class="flyout-filter__header u-show-below@tablet">
        <div class="flyout-filter__overlay" onclick="document.body.classList.toggle(`flyout-filter--active`)"><!-- EMPTY --></div>
        <button class="flyout-filter--toggle button" onclick="document.body.classList.toggle(`flyout-filter--active`)">Filter</button>
    </span>

    <div class="flyout-filter">
        <div role="button" class="flyout-filter--close u-show-below@tablet" onclick="document.body.classList.toggle('flyout-filter--active')"><i class="icon-clear"></i></div>

        <section class="widget widget_facet_stack_widget">
            <h2 class="widget-title">Categorie</h2>
		    <?= facetwp_display('facet', 'categories') ?>
        </section>

        <div class="button-wrapper">
            <a href="#" class="button" id="btnReset" onclick="FWP.reset(); return false;">Reset filter</a>
            <a href="#" class="button" onclick="document.body.classList.toggle(`flyout-filter--active`); return false;">Toon <span class="num_results"> ... resultaten</span> </a>
        </div>
    </div>
</aside>
