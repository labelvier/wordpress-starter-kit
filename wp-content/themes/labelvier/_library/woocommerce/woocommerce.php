<?php
/**
 * Hook into woocommerce filters etc.
 *
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 2019-02-12
 * Time: 19:40
 */

class LabelvierWoocommerce {

    /**
     * Woocommerce constructor.
     * Hook into the place right before the main content
     */
    function __construct() {
        //add specific body class for woocommerce store front
        add_filter( 'body_class', [ $this, 'additional_body_classses' ], 10, 2 );

        //add woocommerce theme support
        add_action( 'after_setup_theme', [ $this, 'add_woocommerce_support' ] );

        // remove woocommerce styling
        add_action('woocommerce_enqueue_styles', [$this, 'remove_woocommerce_styling']);

        // Change opening wrapper for woocommerce
        remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
        add_action( 'woocommerce_before_main_content', [$this, 'woocommerce_output_content_wrapper'], 10 );

        // Move sidebar function, in between closing divs
        remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
        remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
        add_action( 'woocommerce_after_main_content', [$this, 'woocommerce_get_sidebar_and_wrapper_end'], 10 );
    }

    /**
     * adds theme support for woocommerce
     */
    function add_woocommerce_support() {
        add_theme_support( 'woocommerce' );
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
    }

    /**
     * Add additional classes to make styling easy
     *
     * @param $classes
     * @param $class
     *
     * @return array
     */
    function additional_body_classses( $classes, $class ) {
        if ( $this->is_shop() ) {
            $classes[] = 'woocommerce-home';
        }
        if(is_active_sidebar( 'sidebar-shop' )) {
            $classes[] = 'has-sidebar';
        }

        return $classes;
    }

    /**
     * Output the start of the page wrapper.
     */
    function woocommerce_output_content_wrapper() {
        echo '<div class="woocommerce-wrapper">';
        get_sidebar('shop');
        echo '<div class="woocommerce-content">';
    }

    /**
     * Output the end of the page wrapper.
     */
    function woocommerce_get_sidebar_and_wrapper_end() {
        echo '</div> <!-- .woocommerce-content -->';
        echo '</div> <!-- .woocommerce-wrapper -->';
    }

    /**
     * Returns the is_shop function, even when woocommerce is not active.
     *
     * @return bool
     */
    function is_shop() {
        return function_exists( 'is_shop' ) && is_shop();
    }

    /**
     * Remove WooCommerce default styling
     */
    public function remove_woocommerce_styling($enqueue_styles): array {
        // This styling is loaded in the theme
        unset($enqueue_styles['woocommerce-general'], $enqueue_styles['woocommerce-layout'], $enqueue_styles['woocommerce-smallscreen'] );
        return $enqueue_styles;
    }
}

// Global for backwards compatibility.
$GLOBALS['labelvier_woocommerce'] = new LabelvierWoocommerce();


/**
 * Disable WooCommerce block styles (front-end)
 */
function themesharbor_disable_woocommerce_block_styles() {
    wp_dequeue_style( 'wc-blocks-style' );
}
add_action( 'wp_enqueue_scripts', 'themesharbor_disable_woocommerce_block_styles' );

/**
 * Remove woocommerce breadcrumbs
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

/**
 * Remove the page title from the shop page
 */
add_filter( 'woocommerce_show_page_title', '__return_false' );


/**
 * Remove the image zoom on the single product page
 */
function remove_image_zoom_support() {
    remove_theme_support( 'wc-product-gallery-zoom' );
}
add_action( 'wp', 'remove_image_zoom_support', 100 );


/**
 * Change the number of columns in the shop overview
 */
function lw_loop_shop_columns( $columns ): int {
    return 3;
}
add_filter( 'loop_shop_columns', 'lw_loop_shop_columns' );

/**
 * Add wrapper to product loop items
 */
function lw_loop_add_wrapper_start() {
    echo '<div class="product-inner">';
}
add_action( 'woocommerce_before_shop_loop_item', 'lw_loop_add_wrapper_start', 5 );

function lw_loop_add_wrapper_end() {
    echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item', 'lw_loop_add_wrapper_end', 15 );


/**
 * Add count to cart menu item
 * add .cart class to the menu item
 * @return mixed
 */
function mmenu_woo_cart_count( $fragments ) {
    ob_start();
    $cart_count = WC()->cart->cart_contents_count;
    $cart_url   = wc_get_cart_url(); ?>
    <a class="shopping-cart" href="<?= $cart_url ?>">
        <span class="shopping-cart--count"><?= $cart_count ?></span>
    </a>
    <?php $fragments['.mmenu__item.cart a'] = ob_get_clean();
    ob_start();
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'mmenu_woo_cart_count', 10 );
