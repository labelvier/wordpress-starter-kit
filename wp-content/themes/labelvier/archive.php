<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Labelvier
 */

get_header();
?>
    <div class="post__archive">
        <div class="o-container">

            <?php if (have_posts()) : ?>
                <div class="o-layout@tablet">
                    <?php while (have_posts()) : the_post();?>
                        <div class="o-layout__item u-size-6@tablet u-size-4@desktop">
                            <?php get_template_part('template-parts/content', get_post_type()); ?>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php archive_number_pagination();
            else :
                get_template_part('template-parts/content', 'none');
            endif; ?>

        </div>
    </div>

<?php
get_footer();
