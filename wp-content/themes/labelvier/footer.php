<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Labelvier
 */
?>
</main>

<footer class="page-footer">
	<?php get_template_part('template-parts/page-footer'); ?>
</footer>

<?php wp_footer(); ?>
<?php set_query_var('script_placement', 'footer'); get_template_part('template-parts/cookies-scripts'); ?>
<?php get_template_part('template-parts/cookies'); ?>
</body>
</html>
