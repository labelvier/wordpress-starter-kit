<?php
/**
 * Labelvier functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Labelvier
 */

if ( ! function_exists( 'labelvier_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */

	$theme = wp_get_theme();
	define( 'THEME_VERSION', $theme->get('Version') );

	function labelvier_setup() {

        /**
         * Check if the current PHP version is supported
         */
        if (version_compare(phpversion(), '8.0.0', '<=')) {
            echo 'This theme requires PHP 8.0.0 or higher. You are running ' . phpversion() . '. Please upgrade you PHP version.';
            exit;
        }

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Labelvier, use a find and replace
		 * to change 'labelvier' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'labelvier', get_stylesheet_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size('header', 3840, 3840, false); // no cropping with focal
		add_image_size('banner', 1200, 0, false); // no cropping with focal
		add_image_size('item', 800, 800, false); // no cropping with focal

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main-menu' => esc_html__( 'Main Menu', 'labelvier' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		/**
		 * Enable focal point functionality
		 * Notice: needs thumbnail sizes to be the same aspect ratio as original
		 */
		add_filter('labelvier/use_focal_point', '__return_true');

		/**
		 * Enable lazy loading for wp images
		 */
		add_filter('labelvier/use_lazy_loading', '__return_true');

		/**
		 * Enable label vier dashboard
		 */
		add_filter('labelvier/use_custom_admin_dashboard', '__return_true');

        /**
         * Standard disable file modifications on production environments
         */
        add_filter('labelvier/always_allow_file_mods', function($input) {
            return is_develop_environment();
        });
	}
endif;
add_action( 'after_setup_theme', 'labelvier_setup', 9);


if( !function_exists('print_pre')) {
	function print_pre($args) {
		echo '<pre>';
		print_r($args);
		echo '</pre>';
	}
}

/**
 * Force activate a plugin which is needed for the theme
 * @param $plugin
 *
 * @return null
 */
function run_activate_plugin( $plugin ) {
	$current = get_option( 'active_plugins' );
	$plugin = plugin_basename( trim( $plugin ) );

	if ( !in_array( $plugin, $current ) ) {
		$current[] = $plugin;
		sort( $current );
		do_action( 'activate_plugin', trim( $plugin ) );
		update_option( 'active_plugins', $current );
		do_action( 'activate_' . trim( $plugin ) );
		do_action( 'activated_plugin', trim( $plugin) );
	}

	return null;
}
add_action('after_switch_theme', function($oldtheme_name, $oldtheme) {

	$required_plugins = [
		'advanced-custom-fields-pro/acf.php',
		'labelvier/labelvier.php'
	];

	if ( ! function_exists( 'get_plugins' ) ) {
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}
	$plugins = get_plugins();

	$plugins_available = true;
	foreach($required_plugins as $required_plugin) {
		if(!isset($plugins[$required_plugin])) {
			$plugins_available = false;
			break;
		}
	}

	if ( !$plugins_available ) :
		// Info message: Theme not activated
		add_action( 'admin_notices', 'not_activated_admin_notice' );
		function not_activated_admin_notice() {
			echo '<div class="update-nag">';
			_e( 'Theme not activated: this theme requires ACF and the label vier plugins.', 'labelvier' );
			echo '</div>';
		}
		// Switch back to previous theme
		switch_theme( $oldtheme->stylesheet );
		return false;
	else:
		foreach($required_plugins as $required_plugin) {
			run_activate_plugin($required_plugin);
		}
	endif;
}, 10 ,2);



function get_critical_css(): void {
    // Check if critical css file exists
    if (!file_exists(get_template_directory() . '/critical.css')) {
        return;
    }
    $css = file_get_contents(get_template_directory() . '/critical.css');
    // Check if critical css file has content
    if (!$css) {
        return;
    }
    // Return critical css
    echo '<style>' . $css . '</style>';
}
add_action('wp_head', 'get_critical_css', 1);

/**
 * Enqueue scripts and styles.
 */
function labelvier_scripts() {
    wp_enqueue_style( 'labelvier-style', get_stylesheet_uri(), array(), THEME_VERSION );

	wp_enqueue_script( 'labelvier-js', get_template_directory_uri() . '/dist/js/bundle.js', array(), THEME_VERSION, true );
	wp_enqueue_script( 'labelvier-head-js', get_template_directory_uri() . '/dist/js/bundle-head.js', array(), THEME_VERSION, false );

	// add the admin-ajax url as a global var
	wp_localize_script( 'labelvier-head-js', 'labelvier', [
		'adminajax' => admin_url( 'admin-ajax.php' )
	]);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'labelvier_scripts' );

add_action('admin_enqueue_scripts', static function() {
    // Add js for the admin
    wp_enqueue_script( 'labelvier-admin-js', get_template_directory_uri() . '/dist/js/bundle-admin.js', array(), THEME_VERSION, true );
    // Classic editor
    wp_enqueue_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admincss.css', false, THEME_VERSION );
    // Editor style support
    add_editor_style( get_template_directory_uri() . '/admincss.css' );
});


foreach ( glob(get_template_directory() . '/inc/*.php') as $inc ) {
	require $inc;
}
