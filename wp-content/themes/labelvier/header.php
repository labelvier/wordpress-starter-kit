<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Labelvier
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
    <?php set_query_var('script_placement', 'head');  get_template_part('template-parts/cookies-scripts'); ?>
</head>
<body <?php body_class(); ?>>

<header class="page-header">
    <div id="fixed-header-trigger"><!-- Placeholder for fixed header --></div>
	<?php get_template_part('template-parts/page-header'); ?>
</header>

<main class="page-main">
