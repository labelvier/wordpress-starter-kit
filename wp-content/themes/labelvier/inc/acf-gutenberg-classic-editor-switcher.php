<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 31-08-2021
 * Time: 11:09
 */

/**
 * Hide gutenberg via js when loading acf field groups via ajax (dynamic switching via template or other fields)
 *
 * @param $style
 * @param $field_group
 *
 * @return string
 */
function labelvier_hide_gutenberg_on_ajax_switch($style, $field_group) {
	if(defined('DOING_AJAX') && DOING_AJAX) {
		if(labelvier_acf_field_group_contains_hide_gutenberg($field_group)) {
			$template = $_POST['page_template'];
			$style = "</style><script>
                // don't ask for switch if we already had a confirm popup. When somebody is later changing again, we want to show this popup again.
				if(acf.isGutenberg() && (typeof window.askingForSwitch === 'undefined' || Date.now() - window.askingForSwitch > 1)) {
                    // reset page template
                    window.askingForSwitch = Date.now();
                    wp.data.dispatch( 'core/editor' ).undo();
                    window.location = window.location + '&classic_editor&template={$template}';
				} 
			</script><style>.edit-post-layout__metaboxes { display: none !important; }";
		} else {
			$style = "</style><script>
				if(!acf.isGutenberg()) {
                    // hide editor for now
                    document.getElementById('poststuff').style = 'display: none;';
					if(document.getElementById('save-post')) {
		                document.getElementById('save-post').click()
		            } else {
		                document.getElementById('publish').click();
		            }
				} 
			</script><style>";
		}
	}
	return $style;
}
add_action('acf/get_field_group_style', 'labelvier_hide_gutenberg_on_ajax_switch', 10, 2);

/**
 * Hack to force the new template
 */
add_action('admin_footer', function() {
	if(isset($_GET['template'], $_GET['classic_editor'])) : ?>
        <script>
            // hide editor for now
            document.getElementById('poststuff').style = "display: none;";
            // save correct template either in draft or publish
            const template = document.getElementById('page_template');
            template.value = '<?= $_GET['template'] ?>'
            if(document.getElementById('save-post')) {
                document.getElementById('save-post').click()
            } else {
                document.getElementById('publish').click();
            }
        </script>
    <?php else: ?>
        <script>
            const template = document.getElementById('page_template');
            if (template) {
                template.onchange = function (e) {
                    if(document.getElementById('save-post')) {
                        document.getElementById('save-post').click()
                    } else {
                        document.getElementById('publish').click();
                    }
                };
            }
        </script>
	<?php endif;
});

/**
 * Check if we need to hide the gutenberg editor
 *
 * @param $use_block_editor
 * @param $post_type
 *
 * @return false|mixed
 */
function labelvier_acf_hide_gutenberg($use_block_editor, $post_type) {
	if(isset($_GET['classic_editor'])) {
		return false;
	}
	return $use_block_editor;
}
add_action( 'use_block_editor_for_post_type', 'labelvier_acf_hide_gutenberg', 10, 2 );
