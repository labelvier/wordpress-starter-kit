<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 2019-02-13
 * Time: 11:39
 */

/**
 * Make sure child themes will load the json correctly
 */
add_filter( 'acf/settings/save_json', function () {
	return get_stylesheet_directory() . '/acf-json';
} );
add_filter( 'acf/settings/load_json', function ( $paths ) {
	$paths = array( get_template_directory() . '/acf-json' );
	if ( is_child_theme() ) {
		$paths[] = get_stylesheet_directory() . '/acf-json';
	}

	return $paths;
} );


/**
 * Adds the hide gutenberg editor option in the acf fields
 *
 * @param $field
 *
 * @return array
 */
function labelvier_acf_hide_gutenberg_option( $field ) {
	if ( $field['name'] == 'acf_field_group[hide_on_screen]' ) {
		array_unshift( $field['choices'], [ 'gutenberg' => __( "Gutenberg Editor", 'acf' ) ] );
	}

	return $field;
}

add_filter( 'acf/prepare_field', 'labelvier_acf_hide_gutenberg_option', 10, 1 );


/**
 * Checks if the option hide gutenberg editor is enabled in one of the field groups
 *
 * @param $use_block_editor
 * @param $post_type
 *
 * @return bool
 */
function labelvier_acf_parse_hide_gutenberg_option( $use_block_editor, $post_type ) {
	global $post;

	if ( function_exists( 'acf_get_field_groups' ) && isset( $post->ID ) ) {
		// Get field groups for this screen.
		$field_groups = acf_get_field_groups( array(
			'post_id'   => $post->ID,
			'post_type' => $post_type
		) );
		foreach ( $field_groups as $field_group ) {
			if ( labelvier_acf_field_group_contains_hide_gutenberg( $field_group ) ) {
				return false;
			}
		}
	}

	return $use_block_editor;
}

add_action( 'use_block_editor_for_post_type', 'labelvier_acf_parse_hide_gutenberg_option', 10, 2 );

/**
 * @param $field_group
 *
 * @return bool
 */
function labelvier_acf_field_group_contains_hide_gutenberg( $field_group = [] ) {
	return ! empty( $field_group['hide_on_screen'] ) && in_array( 'gutenberg', $field_group['hide_on_screen'], false );
}

/**
 * Gets all available forms in puts them in the select box
 *
 * @param array $field
 *
 * @return array
 */
function show_all_gravity_forms_in_select_field( $field ) {
	if ( is_admin() && is_plugin_active( 'gravityforms/gravityforms.php' ) ) {
		// reset choices
		$field['choices'] = array();
		//fill choices from gravity forms
		$forms = GFAPI::get_forms();
		foreach ( $forms as $form ) {
			$field['choices'][ $form['id'] ] = $form['title'];
		}

		//return field again
		return $field;
	}

	return $field;
}

add_filter( 'acf/load_field/name=gravity_form_select', 'show_all_gravity_forms_in_select_field' );


/**
 * Customise flexible content overview
 */
function my_acf_fields_flexible_content_layout_title( $title ) {
	if (@get_sub_field( 'column_select' )['label']) {
		$column_select = get_sub_field( 'column_select' );
		if ( $column_select['value'] === 'none' ) {
			$title = get_sub_field( 'column_select' )['label'] . ': ';
			$column_1 = get_sub_field('column_1');
			$title .= esc_html( $column_1['select']['label'] );
		}
		if ( $column_select['value'] === '2' ) {
			$title = get_sub_field( 'column_select' )['label'] . ': ';
			$column_1 = get_sub_field('column_1');
			$column_2 = get_sub_field('column_2');
			$title .= esc_html( $column_1['select']['label'] );
			$title .= ' ' . esc_html( $column_2['select']['label'] );
		}
		if ( $column_select['value'] === '3' ) {
			$title = get_sub_field( 'column_select' )['label'] . ': ';
			$column_1 = get_sub_field('column_1');
			$column_2 = get_sub_field('column_2');
			$column_3 = get_sub_field('column_3');
			$title .= esc_html( $column_1['select']['label'] );
			$title .= ' ' . esc_html( $column_2['select']['label'] );
			$title .= ' ' . esc_html( $column_3['select']['label'] );
		}
	}
	return $title;
}
add_filter( 'acf/fields/flexible_content/layout_title/name=blocks', 'my_acf_fields_flexible_content_layout_title', 5, 1 );


/**
 * Set featured image by default and overwrite when specific ACF image is added
 * Remove featured image when specific ACF image is removed and set default again
 *
 * @param $value
 * @param $post_id
 *
 * @return mixed
 */
function acf_set_featured_image( $value, $post_id ) {
	if ( $value !== '' ) {
		set_post_thumbnail( $post_id, $value );
	} else {
		delete_post_thumbnail( $post_id );
	}

	return $value;
}
$acf_set_featured_image_key1 = 'field_635b9bff2ef59';
add_filter( 'acf/update_value/key=' . $acf_set_featured_image_key1, 'acf_set_featured_image', 10, 2 );


/**
 * Not fancy settings styling
 *
 * */
add_action( 'acf/init', 'my_acfe_modules' );
function my_acfe_modules() {
	// Disable Enhanced UI
	acf_update_setting( 'acfe/modules/ui', false );
}

/**
 * Thumbnail previews
 */
add_filter('acf/load_field', function($field) {
	if(is_admin() && get_post_type() === 'acf-field-group') {
		return $field;
	}

	if($field['type'] === 'flexible_content') {
		foreach($field['layouts'] as &$layout) {
			$label = substr($layout['label'], 0);
			$layout['label'] = '<div class="acf-fc-tile">';
			$has_thumbnail = isset( $layout['thumbnail'] ) || false;
			if(!$has_thumbnail) {
				$layout['label'] .= '<div class="acf-fc-title large">' . $label . '</div>';
			} else {
				$layout['label'] .= '<div class="acf-fc-figure"><img src="' .  get_template_directory_uri() . '/dist/images/acf-flexible-layout/' . $layout['thumbnail'] . '" class="acf-fc-thumb"></div><div class="acf-fc-title">' . $label . '<span class="button button-primary button-large">Invoegen</span></div>';
			}
			$layout['label'] .= "</div>";
		}
	}
	return $field;
}, 10, 1);

add_filter('acf/render_field', function($field) {
	if($field['class'] === 'layout-name') {
		echo '</li><li class="acf-preview-thumb">';
				$field_group = acf_get_fields(get_the_ID());
				$path = explode('[', $field['prefix']);
				$item_id =  substr($path[1], 0, -1);
				$layout_id = substr($path[3], 0, -1);
				// find the correct layout
				global $thumbnail;
				$thumbnail = 'placeholder.png';
				array_walk($field_group, function($item) use ( $layout_id, $item_id ) {
					if($item['ID'] == $item_id) {
						array_walk($item['layouts'], function($layout, $layout_key) use ( $layout_id ) {
							if($layout_key == $layout_id && @$layout['thumbnail']) {
								global $thumbnail;
								$thumbnail = $layout['thumbnail'];
							}
						});
					}
				});

				acf_render_field(
					array(
						'type'    => 'text',
						'name'    => 'thumbnail',
						'prefix'  => $field['prefix'],
						'id'      => 'thumbnail',
						'class'   => 'layout-thumbnail',
						'value'   => $thumbnail,
						'placeholder' => 'placeholder.png',
						'prepend' => __( 'Thumbnail', 'acf' ) . ' dist/images/acf-flexible-layout/',
						'ID'      => 0
					)
				);
	}
	return $field;
}, 10, 1);


/**
 * Fix a long-standing issue with ACF, where fields sometimes aren't shown
 * in previews (ie. from Preview > Open in new tab).
 */
if ( class_exists( 'acf_revisions' ) ) {
    // Reference to ACF's <code>acf_revisions</code> class
    // We need this to target its method, acf_revisions::acf_validate_post_id
    $acf_revs_cls = acf()->revisions;
    // This hook is added the ACF file: includes/revisions.php:36 (in ACF PRO v5.11)
    remove_filter( 'acf/validate_post_id', array( $acf_revs_cls, 'acf_validate_post_id', 10 ) );
}


/**
 * Show or hide ACF in admin menu accordingly
 */
add_filter('acf/settings/show_admin', function ($show_admin) {
    if(is_develop_environment()) {
        return true;
    }
    if(!isset($_GET['page'])) {
        return false;
    }
    if($_GET['page'] !== 'acf-settings-updates' ) {
        return false;
    }
    return $show_admin;
});
