<?php

add_action( 'send_headers', 'labelvier_strict_transport_security_header' );
/**
 * Enables the HTTP Strict Transport Security (HSTS) header.
 *
 * @since 1.0.0
 */
function labelvier_strict_transport_security_header() {
	header( 'Strict-Transport-Security: max-age=31536000; includeSubDomains; preload' );
}
