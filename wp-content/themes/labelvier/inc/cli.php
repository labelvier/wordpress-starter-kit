<?php
// create a new wp cli command
add_action( 'cli_init', function () {
	WP_CLI::add_command( 'labelvier hello', 'labelvier_cli_hello' );
});

function labelvier_cli_hello($args, $assoc_args) {
    WP_CLI::success( 'Hello, '. ($args[0] ?? 'World') .'!' );
}