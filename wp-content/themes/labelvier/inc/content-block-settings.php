<?php
/**
 * Get Content Block settings and return values
 */

/**
 * Add classes to content block for assigning background color
 *
 * Use color name as string to set default background color
 * @param string $default_bg_color
 *
 * @return void
 */
function content_block_options_background(
    string $default_bg_color = 'default' // white, gray, gray-light of any custom added color. Check _cb.scss
): void {
    if ($default_bg_color === 'default') {
        $default_bg_color = 'white'; // set default value to 'white'
    }
    if ($block_options = get_sub_field('options')) {
        if ($block_options['background_color'] !== 'default') {
            echo 'cb-bg-color--' . $block_options['background_color'];
        } else {
            echo 'cb-bg-color--' . $default_bg_color;
        }
    } else {
        echo 'cb-bg-color--' . $default_bg_color;
    }
}

/**
 * Add classes to content block for assigning container width, position and alignment
 *
 * Use width, position and alignment as string to set default container width, position and alignment
 * @param string $default_container_width
 * @param string $default_container_alignment
 * @param string $default_text_alignment
 *
 * @return void
 */
function content_block_options_container(
    string $default_container_width = 'default', // full, content, medium, small
    string $default_text_alignment = 'default', // left, center, right
    string $default_container_alignment = 'default' // left, center, right
): void {
    if (($block_options = get_sub_field('options')) && $container_options = $block_options['container_options']) {
        // Default container width options are: default, content, medium, small
        $container_width = $default_container_width; // get default value from function parameter
        if ($default_container_width === 'default') { // if default value is set to 'default'
            $container_width = 'content'; // set default value to 'content'
        }
        if ($container_options['width'] !== 'default') { // if another value is set in block options
            $container_width = $container_options['width']; // set value from block options
        }

        // Default container position options are: center, left, right
        $container_text_alignment = $default_text_alignment; // get default value from function parameter
        if ($default_text_alignment === 'default') { // if default value is set to 'default'
            $container_text_alignment = 'left'; // set default value to 'left'
        }
        if ($container_options['text_alignment'] !== 'default') { // if another value is set in block options
            $container_text_alignment = $container_options['text_alignment']; // set value from block options
        }

        // Default container position options are: center, left, right
        $container_alignment = $default_container_alignment; // get default value from function parameter
        if ($default_container_alignment === 'default') { // if default value is set to 'default'
            $container_alignment = 'center'; // set default value to 'left'
        }
        if ($container_options['alignment'] !== 'default') { // if another value is set in block options
            $container_alignment = $container_options['alignment']; // set value from block options
        }

        // Set container classes
        if ($container_width !== 'full') {
            echo 'o-container';
        }
        if ($container_width !== 'content' && $container_width !== 'full') {
            echo ' u-max-width--' . $container_width;

            if ($container_text_alignment !== 'left') {
                echo ' u-text-align--' . $container_text_alignment;
            }
            if ($container_alignment !== 'center') {
                echo ' u-align--' . $container_alignment;
            }
        }
    }
}


/**
 * Add WYSIWYG tweaks
 */
function labelvier_admin_tweaks()
{
    global $typenow;
    // Check user permissions
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
        return;
    }

    // Check if WYSIWYG is enabled
    if (get_user_option('rich_editing') === 'true') {
        add_filter('mce_external_plugins', 'labelvier_admin_tweaks_plugin');
    }
}

add_action('admin_head', 'labelvier_admin_tweaks');

function labelvier_admin_tweaks_plugin($plugin_array)
{
    $plugin_array['labelvier_admin_tweaks'] = get_template_directory_uri() . '/tinymce-plugin.js';
    return $plugin_array;
}
