<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 12-10-2022
 * Time: 14:47
 */

// create ajax and nopriv ajax call for cookie settings
add_action( 'wp_ajax_nopriv_get_cookie_settings', 'get_cookie_settings' );
add_action( 'wp_ajax_get_cookie_settings', 'get_cookie_settings' );

function get_cookie_settings() {
	get_template_part( 'template-parts/cookie-settings-popup' );
}

// create a shortcode which echoes a href to open the cookie settings popup with an optional title attribute
add_shortcode( 'cookie_settings', 'cookie_settings_shortcode' );
function cookie_settings_shortcode($atts ) {
	$atts = shortcode_atts( array(
		'title' => 'Cookie instellingen',
	), $atts, 'cookie_settings' );
	return '<a href="#" onclick="cookieEngine.showCookiePreferences();return false;" title="' . $atts['title'] . '">' . $atts['title'] . '</a>';
}
