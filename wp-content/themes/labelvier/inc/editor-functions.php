<?php
/**
 * Modify TinyMCE editor to remove H1.
 */
function tiny_mce_remove_unused_formats($init) {
	// Add block format elements you want to show in dropdown
	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre';
	return $init;
}
add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );


/**
 * Remove emoji support
 */
function disable_emoji_feature() {
	// Prevent Emoji from loading on the front-end
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	// Remove from admin area also
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Remove from RSS feeds also
	remove_filter( 'the_content_feed', 'wp_staticize_emoji');
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji');

	// Remove from Embeds
	remove_filter( 'embed_head', 'print_emoji_detection_script' );

	// Remove from emails
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Disable from TinyMCE editor. Currently disabled in block editor by default
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );

	/** Finally, prevent character conversion too
	 ** without this, emojis still work
	 ** if it is available on the user's device
	 */
	add_filter( 'option_use_smilies', '__return_false' );
}

function disable_emojis_tinymce( $plugins ) {
	if( is_array($plugins) ) {
		$plugins = array_diff( $plugins, array( 'wpemoji' ) );
	}
	return $plugins;
}
add_action('init', 'disable_emoji_feature');

/**
 * Tweak TinyMCE editor
 */
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars ) {
	$toolbars['Very Simple' ] = array();
	$toolbars['Very Simple' ][1] = array('bold' , 'italic' , 'underline', 'link' );

    $toolbars['Full' ] = array();
    $toolbars['Full' ][1] = array('formatselect' , 'bold' , 'italic' , 'underline', 'link' , 'bullist' , 'numlist' , 'blockquote' , 'alignleft' , 'aligncenter' , 'alignright' , 'alignjustify' , 'outdent' , 'indent' , 'undo' , 'redo' , 'wp_help' );
    $toolbars['Full' ][2] = array('styleselect' , 'formatselect' , 'fontselect' , 'fontsizeselect' );
    $toolbars['Full' ][3] = array('forecolor' , 'backcolor' , 'charmap' , 'hr' , 'subscript' , 'superscript' , 'strikethrough' , 'removeformat' , 'visualaid' , 'wp_adv' );

	unset( $toolbars['Basic' ] );
	return $toolbars;
}
