<?php
/**
 * Remove default color pallet and add custom colors
 */
function ea_setup() {
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Primary', 'labelvier' ),
			'slug'  => 'primary',
			'color'	=> 'var(--color-primary)',
		),
		array(
			'name'  => __( 'Secondary', 'labelvier' ),
			'slug'  => 'secondary',
			'color'	=> 'var(--color-secondary)',
		),
		array(
			'name'  => __( 'Orange', 'labelvier' ),
			'slug'  => 'orange',
			'color' => '#FFBC49',
		),
		array(
			'name'	=> __( 'Red', 'labelvier' ),
			'slug'	=> 'red',
			'color'	=> '#E2574C',
		),
	) );
}
add_action( 'after_setup_theme', 'ea_setup' );
