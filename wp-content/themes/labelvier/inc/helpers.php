<?php
/**
 * Check if the current environment is the development environment
 * @return bool
 */
function is_develop_environment() {
    return defined( 'WP_ENV' ) && WP_ENV === 'develop';
}
