<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 2019-02-19
 * Time: 11:52
 */


/**
 * Changes logo in the login form to favicon logo
 *
 * @return string|void
 */
function labelvier_login_logo() {
	if (function_exists('get_field') && $loginLogoUrl = @get_field('general', 'options')['admin_login_image']['sizes']['medium_large']) : ?>
        <style>
            #login h1 a, .login h1 a {
                background-image: url(<?= $loginLogoUrl ?>);
                width: 320px;
                height: 136px;
                background-position: center;
                background-size: contain;
                background-repeat: no-repeat;
                padding-bottom: 30px;
            }
        </style>
	<?php endif;
}
add_action( 'login_enqueue_scripts', 'labelvier_login_logo' );

/**
 * Change logo url to home url
 *
 * @return string|void
 */
function labelvier_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'labelvier_login_logo_url' );
