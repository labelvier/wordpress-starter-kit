<?php
/**
 * Monkey Menu
 * @version 1.2.0
 * @author  Nathanael
 * @company Labelvier
 * @website https://labelvier.com
 */

/**
 * Monkey Menu Main Function
 *
 * @param string $current_menu
 * @return array|bool
 */
function wp_get_menu_array( string $current_menu ) {
    global $post;
    // check if $post is an object and has an ID
    $post_id   = $post->ID ?? null;
    $locations = get_nav_menu_locations();
    if ( ! $locations ) {
        echo 'No menu configured for ' . $current_menu;

        return;
    }
    $menu       = wp_get_nav_menu_object( $locations[ $current_menu ] );
    $array_menu = wp_get_nav_menu_items( $menu->term_id );
    $menu       = array();
    $submenu    = array();
    foreach ( $array_menu as $menu_item ) {
        if ( empty( $menu_item->menu_item_parent ) ) {
            $menu = mmenu_create_item( $menu, $menu_item, $post_id );
        }
        else {
            $submenu = mmenu_create_item( $submenu, $menu_item, $post_id );
            $menu[ $menu_item->menu_item_parent ]['children'][ $menu_item->ID ] = $submenu[ $menu_item->ID ];
        }

    }

    return $menu;
}

/**
 * Monkey Menu item attributes
 *
 * @param array $item
 * @param mixed $m
 * @param mixed $post_id
 *
 * @return array
 */
function mmenu_create_item( array $item, mixed $m, mixed $post_id ): array {

    // check if $m is an object and has an ID
    if (!is_object($m) || !isset($m-> ID)){
        return $item;
    }

    $item[ $m->ID] =[
        'ID'           => (int) $m->ID,
        'object_id'    => (int) $m->object_id,
        'title'        => sanitize_text_field($m->title),
        'attr_title'   => sanitize_text_field($m->attr_title ?? ''),
        'url'          => esc_url(rtrim($m->url, '/')),
        'target'       => sanitize_text_field($m->target ?? ''),
        'post_id'      => $post_id,
        'xfn'          => sanitize_text_field($m->xfn ?? ''),
        'classes'      => is_array($m->classes) ? array_map('sanitize_html_class', $m->classes) : [],
        'child_active' => 0,
    ];

    global $wp;
    // get current url
    $current_url = home_url(add_query_arg([], $wp->request));
    // check if current url is the same as the menu item url
    $item[ $m->ID]['active'] = ((int) $m->object_id === $post_id || $current_url === $item[ $m->ID]['url']) ? 1 : 0;

    return $item;
}

/**
 *
 * @param array $item
 *
 * @return string|void
 */
function mmenu_item_custom_classes( array $item ) {
	if ( isset( $item['classes'] ) && is_array( $item['classes'] ) ) {
		$classes = ' ';
		foreach ( $item['classes'] as $class ) {
			$classes .= $class . ' ';
		}

		return $classes;
	}
}

/**
 * Render menu link
 *
 * @param array $item
 *
 * @return void
 */
function render_menu_link( $item ) {
    $active_class = $item['active'] ? 'aria-current="page"' : '';
    $title_attr   = $item['attr_title'] ? 'title="' . esc_attr($item['attr_title']) . '"' : '';
    $rel_attr     = $item['xfn'] ? 'rel="' . esc_attr($item['xfn']) . '"' : '';
    $target       = $item['target'] ?: '_self';

    echo '<a href="' . esc_url($item['url']) . '" target="' . esc_attr($target) . '" ' . $title_attr . ' ' . $rel_attr . ' ' . $active_class . '>';
}

/**
 * Monkey Menu structure
 *
 * @param string $menu_name
 *
 * @return void
 */
function monkey_menu( string $menu_name ) {
	$menu_items = wp_get_menu_array( $menu_name );
	if ( is_array( $menu_items ) ) : ?>
        <div class="mmenu__overlay" onclick="mMenuToggle()"><!-- EMPTY --></div>
        <div class="mmenu__button" onclick="mMenuToggle()" role="button" aria-label="Menu">
            <div class="mmenu__button-lines"><span><!-- --></span><span><!-- --></span><span><!-- --></span></div>
        </div>
        <nav class="mmenu" role="navigation">
            <ul>
                <?php foreach ( $menu_items as $item ) : ?> <!-- mogelijk met $key => als het belangrijk is om op de key van een array te filteren -->
					<?php if ( ! empty( $item['children'] ) ):

						// find ['active'] in array $item['children']
						$child_active = 0;
						foreach ( $item['children'] as $child ) {
							if ( $child['active'] ) {
								$child_active = 1;
							}
						} ?>
                        <li class="mmenu__item mmenu__item-lvl1 mmenu__item--has-children<?php if ( $item['active'] ) : echo ' mmenu__item--current'; endif; ?><?php if ( $child_active ) : echo ' mmenu__item--current-child'; endif; ?><?= mmenu_item_custom_classes( $item ) ?>">
                            <a href="<?= $item['url'] ?>"
								<?php if ( $item['active'] ) : echo 'aria-current="page"'; endif; ?>
                               id="mmenu__item__sub-menu-<?php echo $item['ID']; ?>"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false">
								<?= $item['title'] ?>
                            </a>
                            <div class="mmenu__sub-menu"
                                 aria-labelledby="mmenu__item__sub-menu-<?php echo $item['ID']; ?>">
                                <ul>
                                    <li class="mmenu__item mmenu__item-lvl2 mmenu__item--clone-lvl1<?php if ( $item['active'] ) : echo ' mmenu__item--current'; endif; ?><?= mmenu_item_custom_classes( $item ) ?>">
                                        <?php render_menu_link( $item );
                                            echo esc_html__('Overzichtspagina', 'labelvier');
                                            echo '</a>';
                                        ?>
                                    </li>
									<?php foreach ( $item['children'] as $child ): ?>
                                        <li class="mmenu__item mmenu__item-lvl2<?php if ( $child['active'] ) : echo ' mmenu__item--current'; endif; ?><?= mmenu_item_custom_classes( $child ) ?>">
                                            <?php   render_menu_link( $child );
                                                echo esc_html($child['title']);
                                                echo '</a>';
                                            ?>
                                        </li>
									<?php endforeach; ?>
                                </ul>
                            </div>
                        </li>
					<?php else: ?>
                        <li class="mmenu__item mmenu__item-lvl1<?php if ( $item['active'] ) : echo ' mmenu__item--current'; endif; ?><?= mmenu_item_custom_classes( $item ) ?>">
                            <?php   render_menu_link( $item );
                                echo esc_html($item['title']);
                                echo '</a>';
                            ?>
                        </li>
					<?php endif; ?>
				<?php endforeach; ?>
        </ul>
    </nav>
	<?php
	endif;
}
