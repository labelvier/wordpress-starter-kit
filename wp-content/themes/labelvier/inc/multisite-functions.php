<?php

/**
 * Enable unfiltered_html capability for Editors.
 *
 * @param array $caps The user's capabilities.
 * @param string $cap Capability name.
 * @param int $user_id The user ID.
 *
 * @return array $caps The user's capabilities, with 'unfiltered_html' potentially added.
 */
function km_add_unfiltered_html_capability_to_editors( $caps, $cap, $user_id ) {
	if ( is_multisite() && 'unfiltered_html' === $cap && user_can( $user_id, 'administrator' ) ) {
		$caps = array( 'unfiltered_html' );
	}
	return $caps;
}
add_filter( 'map_meta_cap', 'km_add_unfiltered_html_capability_to_editors', 1, 3 );
