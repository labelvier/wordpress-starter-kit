<?php
///**
// * Generate pages when these do not exist
// */
//
//// If the page home does not exist, create it
//if ( ! get_page_by_path( 'home' ) ) {
//	$home_page = [
//		'post_title'   => 'Home',
//		'post_content' => '',
//		'post_status'  => 'publish',
//		'post_author'  => 1,
//		'post_type'    => 'page',
//	];
//
//	// Insert the page into the database
//	wp_insert_post( $home_page );
//}
//// set frontpage to static page
//update_option( 'show_on_front', 'page' );
//
//// Set the home page as the front page
//update_option( 'page_on_front', get_page_by_path( 'home' )->ID );
//
//
//// Content page
//if ( ! get_page_by_path( 'content' ) ) {
//    $content_page = [
//        'post_title'   => 'Content',
//        'post_content' => '',
//        'post_status'  => 'publish',
//        'post_author'  => 1,
//        'post_type'    => 'page',
//    ];
//
//    // Insert the page into the database
//    wp_insert_post( $content_page );
//}
//
///**
// * Generate menu when this does not exist
// */
//// If main-menu does not exist, create it
//if ( ! wp_get_nav_menu_object( 'main-menu' ) ) {
//	$menu_id = wp_create_nav_menu( 'Main Menu' );
//
//	// Set up default menu items
//	wp_update_nav_menu_item( $menu_id, 0, [
//		'menu-item-title'   => 'Home',
//		'menu-item-classes' => 'home',
//		'menu-item-url'     => home_url( '/' ),
//		'menu-item-status'  => 'publish',
//	] );
//    wp_update_nav_menu_item( $menu_id, 0, [
//        'menu-item-title'   => 'Content',
//        'menu-item-classes' => 'content',
//        'menu-item-url'     => home_url( '/content' ),
//        'menu-item-status'  => 'publish',
//    ] );
//	// Set the menu to the main-menu
//	$locations              = get_theme_mod( 'nav_menu_locations' );
//	$locations['main-menu'] = $menu_id;
//	set_theme_mod( 'nav_menu_locations', $locations );
//}
