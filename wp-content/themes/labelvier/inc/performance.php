<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 28-04-2022
 * Time: 13:33
 */

function labelvier_defer_css_load($html, $handle, $href, $media) {
	if (is_admin()) {
		return $html;
    }

	$defer_handles = [
		'dashicons',
		'admin-bar',
		'labelvier-style-css',
		'wp-block-library',
		'wp-blocks-vendor-style',
		'wc-blocks-style',
		'wc-blocks-vendors-style',
		'global-styles',
		'classic-theme-styles',
		'yoast-seo-adminbar',
		'sbi_styles',
		'gform_theme_components',
		'gravity_forms_theme_reset',
		'gravity_forms_theme_foundation',
		'gravity_forms_theme_framework',
		'gravity_forms_orbital_theme',
		'gravity_forms_theme',
		'gform_theme_ie11',
		'gform_basic',
		'gform_theme',
        'labelvier-style'
	];


    if (function_exists('is_woocommerce_activated') && !is_woocommerce() && !is_product() && !is_cart() && !is_checkout() && !is_account_page()) {
        $defer_handles[] = 'woocommerce-layout';
        $defer_handles[] = 'woocommerce-smallscreen';
		$defer_handles[] = 'custom-woocommerce';
        $defer_handles[] = 'woocommerce-general';
    }

	if(! in_array($handle, $defer_handles)) {
		return $html;
	}
	$html = <<<EOT

<link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'" id='$handle' href='$href' />
EOT;
	return $html;
}
add_filter( 'style_loader_tag', 'labelvier_defer_css_load', 10, 4 );

/**
 * Alter script tag(s) to async/defer it/them.
 *
 * @see https://developer.wordpress.org/reference/hooks/script_loader_tag/
 *
 * @param string $tag The <script> tag for the enqueued script.
 * @param string $handle The script's registered handle.
 * @param string $url The script's source URL.
 * @return string $tag The (modified) <script> tag for the enqueued script.
 **/
function defer_parsing_of_js($tag, $handle, $src) {
    // don't defer if we're in the admin
    if(is_admin()) {
        return $tag;
    }
	// Defer WordPress Popular Posts' script
	$defer_handles = [
		'admin-bar',
		'swiper-js',
		'jivochat',
		'sbi_scripts',
		'jivo-custom',
		'lottie-js',
		'font-awesome-official',
		'hoverintent-js',
		'labelvier-js',
		'jquery',
		'jquery-migrate',
	];
	if (is_user_logged_in()) {
		// make sure the backend is not affected
		$defer_handles = array_diff($defer_handles, ['jquery', 'jquery-migrate']);
	}

	if ( in_array($handle, $defer_handles) ) {
		return str_replace(' src', ' defer src', $tag);
	}

	// Return original tag
	return $tag;
}
add_filter('script_loader_tag', 'defer_parsing_of_js', 10, 3);

/**
 * Check if Gutenberg blocks are present on page and dequeue styling if there are none
 * SEO optimisation
 * @return void
 */
function check_if_gutenberg_is_present() {
    if (!has_blocks()) {
        wp_dequeue_style( 'wp-block-library' );
        wp_dequeue_style( 'wp-block-library-theme' );
	    wp_dequeue_style( 'wc-blocks-style' );
	    wp_dequeue_style( 'global-styles' );
	    wp_dequeue_style( 'classic-theme-styles' );
    }
}
add_action( 'wp_enqueue_scripts', 'check_if_gutenberg_is_present', 99, 0 );
