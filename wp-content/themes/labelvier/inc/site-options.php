<?php
/**
 * Option pages
 *
 * @package Labelvier
 */


/**
 * Remove link to customizer in frontend admin bar
 */
function labelvier_before_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'customize' );
}
add_action( 'wp_before_admin_bar_render', 'labelvier_before_admin_bar_render' );


/**
 * Add password protection to the site
 * Shows form when loading WordPress content
 * ACF block content pages are protected in the blocks-page loop
 */
add_filter('the_content', 'enable_password_protect_for_content');
function enable_password_protect_for_content($the_content) {
	if(post_password_required()) {
		$the_content = get_the_password_form();
	}
	return $the_content;
}


/**
 * Fallback featured image for pages and posts
 * Also makes sure an og:image tag is generated when sharing a page or post on social media, even when there is no featured image set.
 */
add_filter('post_thumbnail_id', function($thumbnail_id, $post) {
	if (is_admin() || (defined('REST_REQUEST') && REST_REQUEST)) {
		return $thumbnail_id;
	}
	if(!$thumbnail_id && apply_filters('labelvier_fallback_featured_image', true, $post)) {
		$thumbnail_id = get_field( 'general', 'options' )['featured_image_default'] ?? null;
	}
	return $thumbnail_id;
}, 10, 2);


/**
 * Remove title prefix from archive/tag/author/etc
 */
add_filter('get_the_archive_title', function ($title) {
	if (is_category()) {
		$title = single_cat_title('', false);
	} elseif (is_tag()) {
		$title = single_tag_title('', false);
	} elseif (is_author()) {
		$title = '<span class="vcard">' . get_the_author() . '</span>';
	} elseif (is_tax()) { //for custom post types
		$title = sprintf(__('%1$s'), single_term_title('', false));
	} elseif (is_post_type_archive()) {
		$title = post_type_archive_title('', false);
	}
	return $title;
});

/**
 * Disallow file modifications by default
 * Except when in development environment
 */
add_filter( 'file_mod_allowed', static function () {
    return apply_filters('labelvier/always_allow_file_mods', false);
}, 9);
