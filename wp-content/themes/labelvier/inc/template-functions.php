<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Labelvier
 */


/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function labelvier_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'labelvier_pingback_header' );


/**
 * Add Numbered Pagination
 */
function archive_number_pagination() {
	global $wp_query;
	$big = 9999999; // need an unlikely integer
	echo '<nav class="pagination">';
	echo paginate_links( array(
		'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'  => '?paged=%#%',
		'current' => max( 1, get_query_var( 'paged' ) ),
		'total'   => $wp_query->max_num_pages
	) );
	echo '</nav>';
}

/**
 * Customisable excerpt
 *
 * @param int $count
 * @param int|null $post_id
 * @param string $end
 *
 * @return string
 */

function labelvier_excerpt(int $count = 200, int $post_id = null, string $end = ' ...' ) {
	global $post;
	$current_post_id = $post_id ?: $post->ID ;
	$excerpt = get_the_excerpt($current_post_id);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $count);
//	$excerpt = substr($excerpt, 0, strrpos($excerpt, " "));
	if (strlen($excerpt) >= $count) {
		$excerpt .= $end;
	}
	return $excerpt;
}


/**
 * Remove "Category" from category title
 */
function prefix_category_title( $title ) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	}
	return $title;
}
add_filter( 'get_the_archive_title', 'prefix_category_title' );
