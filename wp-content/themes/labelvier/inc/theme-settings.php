<?php
/**
 * Theme Settings pages.
 *
 * @package Labelvier
 */

/**
 * Init the acf settings page
 */
function labelvier_init_theme_settings_pages() {
	if ( function_exists( 'acf_add_options_sub_page' ) ) {
		$option_page = acf_add_options_sub_page(
			array(
				'page_title'  => 'Theme Settings',
				'menu_title'  => 'Theme Settings',
				'menu_slug'   => 'theme-settings',
				'parent_slug' => 'themes.php',
				'capability'  => 'manage_options',
				'redirect'    => false,
				'position'    => 9
			)
		);
	}
}
add_action('acf/init', 'labelvier_init_theme_settings_pages', 10);

/**
 * Replace site_icon value with acf site_favicon value
 */
add_filter( 'acf/update_value/name=site_favicon', function ( $value ) {
	update_option( 'site_icon', $value );

	return $value;
}, 10, 1 );


// after saving acf options, clear the cache
add_action( 'acf/save_post', function () {
	$screen = get_current_screen();
	if ( strpos( $screen->id, "theme-settings" ) ) {
		// clear the savvii cache
		do_action('warpdrive_cache_flush');
	}
} );

/*
 * add theme settings to the admin bar
 */
add_action( 'admin_bar_menu', function ( $wp_admin_bar ) {
    $wp_admin_bar->add_node(
        array(
            'id'    => 'theme-settings',
            'title' => 'Theme Settings',
            'href'  => admin_url( 'admin.php?page=theme-settings' ),
            'parent' => 'appearance',
        )
    );
}, 999 );

// if we are on the theme settings page show a warning admin notice
add_action('admin_notices', static function() {
    if (get_current_screen()->id === 'appearance_page_theme-settings') {

        // check if we have css in the custom css field (via customizer)
        $custom_css = wp_get_custom_css();
        if($custom_css) {
            ?>
            <div class="notice notice-warning">
                <p>Let op! Er is custom CSS ingesteld via de customizer. Wijzigingen hier kunnen invloed hebben op de weergave van de website.</p>
                <p><a href="<?= admin_url('customize.php?autofocus[section]=custom_css') ?>">Ga naar de customizer</a></p>
            </div>
            <?php
        }
    }
});