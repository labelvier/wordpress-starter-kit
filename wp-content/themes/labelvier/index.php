<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Labelvier
 *
 * index.php
 *
 * - archive.php
 * -- archive-$posttype.php
 * -- author.php
 * -- category.php
 * -- date.php
 * -- tag.php
 *
 * - single.php
 * -- single-$posttype.php
 * -- single-post.php
 *
 * - page.php
 * -- front-page.php // Site front-page
 * -- page-$id.php
 * --- page-$slug.php
 *
 * - home.php // Blog posts index page
 *
 * - 404.php
 * - search.php
 */

get_header();?>

    <div class="o-container u-max-width--large">
        <?php if (have_posts()) : ?>
            <div class="o-layout@tablet">
                <?php while (have_posts()) : the_post();?>
                    <?php if(! have_rows('blocks')):?>
                        <div class="o-layout__item u-size-6@tablet u-size-4@desktop">
                            <?php get_template_part('template-parts/content', get_post_type()); ?>
                        </div>
                    <?php else:?>
                        <?php get_template_part('template-parts/blocks', 'page'); ?>
                    <?php endif;?>
                <?php endwhile; ?>
            </div>
            <?= archive_number_pagination();
        else :
            get_template_part('template-parts/content', 'none');
        endif; ?>
    </div>

<?php get_footer();
