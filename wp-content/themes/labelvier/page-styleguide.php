<?php
/**
 * Styleguide
 */

get_header();
?>

    <style>
        table td {
            padding: 10px;
            text-align: center;
            width: 25%;
            vertical-align: top;
        }

        .styleguide-block {
            padding: 20px 0;
        }
    </style>
    <div class="template-layout">

        <div class="styleguide-block o-container">
            <h2>Buttons</h2>

            <div class="o-layout">
                <div class="o-layout__item">
                    <a href="" class="button">href</a><br/>
                </div>
                <div class="o-layout__item">
                    <button>button</button>
                </div>
                <div class="o-layout__item">
                    <input value="input[submit]" type="submit"/><br/>
                </div>
            </div>
            <br /><br />
            <div class="o-layout">
                <div class="o-layout__item">
                    <a href="" class="button button--outline">href</a><br/>
                </div>
                <div class="o-layout__item">
                    <button class="button--outline">button</button>
                </div>
                <div class="o-layout__item">
                    <input value="input[reset]" type="reset"/><br/>
                </div>
            </div>
        </div>

    </div> <!-- .template-layout -->

<?php
get_template_part( 'template-parts/blocks', 'page' );
get_footer();
