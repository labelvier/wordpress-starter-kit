<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package labelvier
 */

get_header();

while ( have_posts() ) :
	the_post();

	if ( ! have_rows( 'blocks' ) ):
		get_template_part( 'template-parts/content', get_post_type() );
	else:
		get_template_part( 'template-parts/blocks', 'page' );

        if ( class_exists( 'WooCommerce' ) ) :
            if ( is_shop() || is_product_category() || is_product_tag() || is_product() || is_cart() || is_checkout() || is_account_page() || is_wc_endpoint_url() ) :
                get_template_part( 'template-parts/content', get_post_type() );
            endif;
        endif;
	endif;
endwhile;

get_footer();
