<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Labelvier
 */

get_header();
?>

    <div class="o-container">

		<?php if ( have_posts() ) : ?>

			<?= get_search_form() ?>

            <p class="search-form__text"><?= get_field( 'pages', 'option' )['search_content_found'] ?><span> <?= get_search_query() ?></span></p>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content' );

			endwhile;

			the_posts_navigation();

		else : ?>

			<?php get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

    </div>

<?php
get_footer();
