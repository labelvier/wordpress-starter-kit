<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Labelvier
 */

get_header();
?>
    <div class="post__single-content">

		<?php while ( have_posts() ) :
			the_post();

			if ( !have_rows( 'blocks' ) ):
				echo '<div class="o-container">';
				get_template_part( 'template-parts/content', get_post_type() );
				echo '</div>';
			else:
				get_template_part( 'template-parts/blocks', 'page' );
			endif;
		endwhile; ?>

    </div>
<?php
get_footer();
