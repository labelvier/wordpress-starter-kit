/**
 * Import the used scripts to load in the footer
 * <!-- HERE --> </body>
 */
import './components/detect-scroll-from-top';
import './components/monkeyMenu';

import GLightbox from 'glightbox';
window.GLightbox = GLightbox;

// import Swiper, {
//     Navigation,
//     Pagination
// } from 'swiper';
//
// window.Swiper = Swiper.use([
//     Navigation,
//     Pagination
// ]);
