/*\
|*|
|*|  :: cookies.js ::
|*|
|*|  A complete cookies reader/writer framework with full unicode support.
|*|
|*|  Revision #3 - July 13th, 2017
|*|
|*|  https://developer.mozilla.org/en-US/docs/Web/API/document.cookie
|*|  https://developer.mozilla.org/User:fusionchess
|*|  https://github.com/madmurphy/cookies.js
|*|
|*|  This framework is released under the GNU Public License, version 3 or later.
|*|  http://www.gnu.org/licenses/gpl-3.0-standalone.html
|*|
|*|  Syntaxes:
|*|
|*|  * docCookies.setItem(name, value[, end[, path[, domain[, secure]]]])
|*|  * docCookies.getItem(name)
|*|  * docCookies.removeItem(name[, path[, domain]])
|*|  * docCookies.hasItem(name)
|*|  * docCookies.keys()
|*|
\*/

window.docCookies = {
    getItem: function (sKey) {
        if (!sKey) {
            return null;
        }
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    },
    setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
        if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
            return false;
        }
        var sExpires = "";
        if (vEnd) {
            switch (vEnd.constructor) {
                case Number:
                    sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                    /*
                    Note: Despite officially defined in RFC 6265, the use of `max-age` is not compatible with any
                    version of Internet Explorer, Edge and some mobile browsers. Therefore passing a number to
                    the end parameter might not work as expected. A possible solution might be to convert the the
                    relative time to an absolute time. For instance, replacing the previous line with:
                    */
                    /*
                    sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; expires=" + (new Date(vEnd * 1e3 + Date.now())).toUTCString();
                    */
                    break;
                case String:
                    sExpires = "; expires=" + vEnd;
                    break;
                case Date:
                    sExpires = "; expires=" + vEnd.toUTCString();
                    break;
            }
        }
        document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
        return true;
    },
    removeItem: function (sKey, sPath, sDomain) {
        if (!this.hasItem(sKey)) {
            return false;
        }
        document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
        return true;
    },
    hasItem: function (sKey) {
        if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
            return false;
        }
        return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
    },
    keys: function () {
        var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
        for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) {
            aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]);
        }
        return aKeys;
    }
};

window.cookieEngine = {
    /**
     * Add accepted cookie and reload the page
     *
     * @param array acceptedCookies
     */
    setAcceptCookie: function(acceptedCookies) {
        // check if level2 is in the array
        if (acceptedCookies.indexOf('level2') !== -1) {
            docCookies.setItem('level2Accepted', JSON.stringify(true), 60 * 60 * 24 * 365 * 99, '/');
        } else {
            docCookies.removeItem('level2Accepted', '/');
        }
        // check if level1 is in the array
        if (acceptedCookies.indexOf('level1') !== -1) {
            docCookies.setItem('level1Accepted', JSON.stringify(true), 60 * 60 * 24 * 365 * 99, '/');
        } else {
            docCookies.removeItem('level1Accepted', '/');
        }

        // remember that the user has accepted the cookies
        docCookies.setItem('cookieAccepted', JSON.stringify(true), 60 * 60 * 24 * 365 * 99, '/');

        // hide the cookie notice and load the scripts
        if(document.getElementById('cookie-notice')) {
            document.getElementById('cookie-notice').style.display = 'none';
        }
        if(typeof this.load_custom_scripts === 'function') {
            this.load_custom_scripts();
        }
    },
    /**
     * Loop through all footerScripts array elements and load them to the DOM
     *
     */
    load_custom_scripts: function() {
        const scriptLocations = ['headScripts', 'footerScripts'];
        const level2Accepted = docCookies.getItem('level2Accepted');
        const level1Accepted = docCookies.getItem('level1Accepted');
        scriptLocations.forEach(function(location) {
            // loop through all headScripts array elements and load them to the DOM
            if (typeof window[location] !== 'undefined') {
                const scriptsToDelete = [];
                window[location].forEach(function(script) {
                    // load the script if the type matches the accepted cookie
                    if(script.code && ((script.type === 'level1' && level1Accepted) || (script.type === 'level2' && level2Accepted))) {
                        const frag = document.createRange().createContextualFragment(script.code);
                        document.head.appendChild(frag);
                        // delete the script from the array
                        scriptsToDelete.push(window[location].indexOf(script));
                    }
                });
                // loop through the scriptsToDelete array and clear the scripts from the array to prevent double triggering
                scriptsToDelete.forEach(function(index) {
                    window[location][index].code = undefined;
                });
            }
        });
    },

    /**
     * Save the preferences from the popup
     */
    saveCookiePreferences:  function() {
        const acceptedCookies = [];
        const level2 = document.getElementById('level2_tracking');
        const level1 = document.getElementById('level1_tracking');
        if (level2 && level2.checked) {
            acceptedCookies.push('level2');
        }
        if (level1 && level1.checked) {
            acceptedCookies.push('level1');
        }
        this.setAcceptCookie(acceptedCookies);
        // hide the popup and cookie preferences
        document.getElementById('cookie-settings').innerHTML = '';
        if(document.getElementById('cookie-notice')) {
            document.getElementById('cookie-notice').remove();
        }
    },

    showCookiePreferences: function() {
        // do an ajax call with the action get_cookie_settings
        const xhr = new XMLHttpRequest();
        xhr.open('GET', labelvier.adminajax + '?action=get_cookie_settings', true);
        xhr.onload = function () {
            if (xhr.status === 200) {
                document.getElementById('cookie-settings').innerHTML = xhr.responseText;
                if(document.getElementById('cookie-notice')) {
                    document.getElementById('cookie-notice').style.visibility = 'hidden';
                }
            }
        }
        xhr.send();

    },

    goBackFromCookiePreferences: function() {
        document.getElementById('cookie-settings').innerHTML = '';
        if(document.getElementById('cookie-notice')) {
            document.getElementById('cookie-notice').style.visibility = 'visible';
        }
    }

}



// Load custom scripts
window.addEventListener('load', function() {
    cookieEngine.load_custom_scripts();
});

