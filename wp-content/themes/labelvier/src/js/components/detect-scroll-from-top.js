document.addEventListener("DOMContentLoaded", () => {
    const triggerElement = document.querySelector("#fixed-header-trigger");

    if (!triggerElement) {
        console.warn("Trigger element #fixed-header-trigger not found.");
        return;
    }

    const observer = new IntersectionObserver(
        ([entry]) => {
            document.body.classList.toggle("header--solid", entry.intersectionRatio !== 1);
        },
        {
            threshold: 1,
            rootMargin: "100px 0px 0px 0px", // Trigger 100px from the top
        }
    );

    observer.observe(triggerElement);
});
