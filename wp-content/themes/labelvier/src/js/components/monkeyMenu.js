/**
 * Monkey Menu
 * Company: LabelVier
 * Author: Nathanael
 * Version: 1.2.0
 * License: MIT
 */

const mMenuBreakpoint = 768; // Tablet breakpoint
const CLASS_SUBMENU_ACTIVE = 'submenu-active';
const CLASS_TOUCH_ENABLED = 'mmenu-touch-enabled';
const CLASS_MENU_OPEN = 'mmenu--open';

function isTouchScreenDevice() {
    return 'ontouchstart' in window || navigator.maxTouchPoints;
}

document.addEventListener("DOMContentLoaded", function () {
    // If touch device, add class to body
    if (isTouchScreenDevice()) {
        document.querySelector('body').classList.add(CLASS_TOUCH_ENABLED);
    }

    mMenuActivate();
    /*
    let resizeTimeout;
    window.addEventListener("resize", function () {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function () {
            location.reload();
        }, 250);
    });
     */

});

// Activate the menu
mMenuActivate = function () {
    const isSmallScreen = window.innerWidth < mMenuBreakpoint;
    if (isTouchScreenDevice() || isSmallScreen) {
        mMenuSubmenuOnClick();
    } else {
        mMenuSubmenuOnHover();
        mMenuSubmenuOnTab();
    }
}

// Submenu menu on click for touch enabled devices
mMenuSubmenuOnClick = function () {
    document.querySelector('body').classList.add(CLASS_TOUCH_ENABLED);
    document.querySelectorAll(".mmenu__item--has-children > a").forEach((element) =>  {
        element.addEventListener("click", handleMenuClick);
    });
}

// handles the click event on the menu item
handleMenuClick = function(event) {
    event.preventDefault();
    const parent = this.parentNode;
    if(parent.classList.contains(CLASS_SUBMENU_ACTIVE)) {
        mMenuSubmenuSlideUp(parent.querySelector(".mmenu__sub-menu"));
    } else {
        document.querySelectorAll(".mmenu__item--has-children").forEach((element) => {
            mMenuSubmenuSlideUp(element.querySelector(".mmenu__sub-menu"));
        });
        mMenuSubmenuSlideDown(parent.querySelector(".mmenu__sub-menu"));
    }
}



// Submenu menu on hover for desktop
mMenuSubmenuOnHover = function () {
    document.querySelectorAll(".mmenu__item--has-children").forEach(function (element) {
        element.addEventListener("mouseenter", function () {
            mMenuSubmenuSlideDown(this.querySelector(".mmenu__sub-menu"));
        });
        element.addEventListener("mouseleave", function () {
            mMenuSubmenuSlideUp(this.querySelector(".mmenu__sub-menu"));
        });
    });
}

// Submenu menu on tab for desktop
mMenuSubmenuOnTab = function () {
    document.querySelectorAll(".mmenu__item--has-children").forEach(function(element) {
        element.querySelector("a").addEventListener("focus", function(event) {
            mMenuSubmenuSlideDown(element.querySelector(".mmenu__sub-menu"));
        });
        element.querySelector(".mmenu__sub-menu").addEventListener("focusout", function(event) {
            if (!element.querySelector(".mmenu__sub-menu").contains(event.relatedTarget)) {
                mMenuSubmenuSlideUp(this);
            }
        });
    })
}

// Slide animation submenu
mMenuSubmenuSlideDown = function (element) {
    if (!element) return;
    element.style.height = element.scrollHeight + "px";
    element.parentNode.classList.add(CLASS_SUBMENU_ACTIVE);
}
mMenuSubmenuSlideUp = function (element) {
    if (!element) return;
    element.style.height = "0px";
    element.parentNode.classList.remove(CLASS_SUBMENU_ACTIVE);
}

// Is used by frontend to open and close the menu
mMenuToggle = function () {
    document.querySelectorAll(".mmenu__item--current-child").forEach(function (element) {
        mMenuSubmenuSlideDown(element.querySelector(".mmenu__sub-menu"));
    });
    if (document.body.classList.contains(CLASS_MENU_OPEN)) {
        document.querySelectorAll(".mmenu__item--has-children.submenu-active").forEach(function (element) {
            mMenuSubmenuSlideUp(element.querySelector(".mmenu__sub-menu"));
        });
    }
    document.body.classList.toggle(CLASS_MENU_OPEN);
};
