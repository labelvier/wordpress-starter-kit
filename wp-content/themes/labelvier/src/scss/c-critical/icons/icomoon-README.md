# Icomoon

## Using Icomoon first time or add/change existing
- Go to https://icomoon.io/app (manage projects)
- Import icons/icomoon.json with Import Project
- Add icons or make changes and generate font
- Export icons and unpack .zip file
- Copy the contents of style.scss and paste/overwrite icons/_icomoon.scss
- Copy the contents of variables.scss and paste/overwrite icons/_variables.scss
- Go to https://icomoon.io/app (manage projects) and download this changed project .json file
- Overwrite the existing icomoon.json
