<?php if (have_rows('blocks')): ?>
    <div class="page-blocks">
		<?php if (post_password_required()) :
			echo '<div class="password-protected-form"><div class="o-container">';
			echo get_the_password_form();
			echo '</div></div>';
		else :
		    while (have_rows('blocks')) : the_row();
                get_template_part('template-parts/blocks/' . get_row_layout());
            endwhile;
        endif;?>
    </div>
<?php endif; ?>
