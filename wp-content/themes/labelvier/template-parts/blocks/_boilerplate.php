<section class="cb cb-text-and-image <?php content_block_settings_classes(); ?>">
    <div class="cb-anchor" id="cb<?= get_row_index() ?>"></div>

    <div class="<?php content_block_settings_container() ?>">
        <div class="o-layout m-layout--align-middle">
            <div class="o-layout__item u-size-12 u-size-6@tablet">
                <?php get_template_part('template-parts/blocks/components/content-title-text-buttons') ?>
            </div>
            <div class="o-layout__item u-size-12 u-size-6@tablet">
                <?php get_template_part('template-parts/blocks/components/content-image') ?>
            </div>
        </div>
    </div>
</section>
