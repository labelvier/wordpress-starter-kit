<?php if ( $args ) :
	// If arguments are passed, use them
	// for example, get_template_part('template-parts/blocks/components/content-buttons', false, get_sub_field('buttons))
	$text_data = $args;

	// if $text_data has ['title'], expect it to be a single link and put it in the correct array structure
	if ( array_key_exists( 'title', $text_data ) ) :
		$text_data = array( 'buttons' => array( array( 'link' => $text_data ) ) );
	else :
		$text_data = array( 'buttons' => $text_data );
	endif;
else :
	// If no arguments are passed, get data directly from ACF field
	$text_data = get_sub_field( 'text_data' );
endif; ?>

<?php if ( isset($text_data['buttons']) && $args ): ?>
    <div class="cb__content__buttons">
		<?php foreach ( $text_data['buttons'] as $button ) : ?>
			<?php if ( $link = $button['link'] ):
				$button_type = $button['type'] ?? 'button'; ?>
                <a class="<?= $button_type ?>" href="<?= $link['url'] ?>" role="button"
                   target="<?= $link['target'] ?: '_self' ?>">
					<span><?= $link['title'] ?></span>
                </a>
			<?php endif; ?>
		<?php endforeach; ?>
    </div>
<?php endif; ?>
