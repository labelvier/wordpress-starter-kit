<?php
if ($args) {
    // If arguments are passed, use them
    $image_data = $args; // for example, get_template_part('template-parts/blocks/components/content-title-text-buttons', false, $item['image_data'])
} else {
    // If no arguments are passed, use the ACF fields
    $image_data = get_sub_field('image_data');
}

$image = $image_data['image'];
$image_behaviour = $image_data['image_behaviour'];
$image_object_fit = $image_behaviour['image_object_fit'];
$video = $image_data['video'] ?? false;
$image_height = null;
if ($image_object_fit === 'cover') {
    $image_height = $image_behaviour['image_size'];
}

if ($image): ?>
    <div class="cb__image <?php if ($image_height) : echo 'u-image-height--' . $image_height; endif; ?> u-image-behaviour--<?= $image_object_fit ?>">
        <?php if ($video['enable_video']) : ?>
            <a href="<?= $video['url'] ?>" target="_blank" class="glightbox" aria-label="Bekijk deze video">
                <i class="icon-play"></i>
                <figure class="u-image-background-container">
                    <?= wp_get_attachment_image_with_focal_point($image, 'banner') ?>
                </figure>
            </a>
            <script type="text/javascript">
                window.addEventListener('load', function () {
                    const lightbox<?= get_row_index() ?> = GLightbox();
                });
            </script>
        <?php else : ?>
            <figure class="u-image-background-container">
                <?= wp_get_attachment_image_with_focal_point($image, 'banner') ?>
            </figure>
        <?php endif; ?>
    </div>
<?php endif; ?>
