<?php if ( $args ) :
	$text_data = $args;
else :
	// If no arguments are passed, get data directly from ACF field
	$text_data = get_sub_field( 'text_data' );
endif; ?>

<?php if ( isset( $text_data['text'] ) ): ?>
    <div class="cb__content__text wysiwyg-styling"><?= $text_data['text'] ?></div>
<?php endif; ?>
