<?php if ( $args ) :
	// If arguments are passed, use them
	// for example, get_template_part('template-parts/blocks/components/content-title-text-buttons', false, $item['text_data'])
	$text_data = $args;
else :
	// If no arguments are passed, use the ACF fields
	$text_data = get_sub_field( 'text_data' );
endif;
// Make sure this field is a Link called 'buttons' or a cloned field called 'buttons' in the group 'text_data'
$buttons = $text_data['buttons'] ?? null; ?>

<div class="cb__content">
	<?php get_template_part( 'template-parts/blocks/components/content-title', false, $text_data ) ?>

    <?php get_template_part( 'template-parts/blocks/components/content-text', false, $text_data ) ?>

	<?php get_template_part( 'template-parts/blocks/components/content-buttons', false, $buttons ) ?>
</div>


