<?php if ( $args ) :
	$text_data = $args;
else :
	// If no arguments are passed, get data directly from ACF field
	$text_data = get_sub_field( 'text_data' );
endif; ?>

<?php if ( isset( $text_data['title'] ) && $text_data['title'] ): ?>
        <h2 class="cb__content__title"><?= $text_data['title'] ?></h2>
<?php endif; ?>
