<section class="cb cb-cta <?php content_block_options_background('white'); ?>">
    <div class="cb-anchor" id="cb<?= get_row_index() ?>"></div>


    <div class="o-container--wrapper">
        <div class="<?php content_block_options_container('content') ?>">
            <?php if ($title = get_sub_field('title')) : ?>
                <h2 class="cb-cta__title cb__heading-title"><?= $title ?></h2>
            <?php endif; ?>

            <div class="o-layout m-layout--justify-center">
                <?php $items = get_sub_field('items');
                foreach ($items as $item) : ?>
                    <div class="o-layout__item u-size-6 u-size-3@tablet">
                        <div class="cb-cta-item">
                            <a href="<?= $item['link']['url'] ?>" target="<?= $item['link']['target'] ?: '_self' ?>">
                                <figure class="u-image-background-container">
                                    <?= wp_get_attachment_image_with_focal_point($item['image'], 'item') ?>
                                </figure>
                                <h2 class="cb-cta-item__title"><?= $item['link']['title'] ?></h2>
                            </a>
                        </div>
                    </div>
                    <?php wp_reset_postdata();
                endforeach; ?>
            </div>
        </div>
    </div>
</section>
