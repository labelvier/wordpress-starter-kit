<?php if ($args) :
    // If arguments are passed, use them
    // for example, get_template_part('template-parts/blocks/components/content-title-text-buttons', false, $item['text_data'])
    $form_title = $args['form_title'];
    $form_id = $args['gravity_form_select'];
    $text = $args['text'];
else :
    // If no arguments are passed, use the ACF fields
    $form_title = get_sub_field('form_title');
    $form_id = get_sub_field('gravity_form_select');
    $text = get_sub_field('text');
endif; ?>

<section class="cb cb-form <?php content_block_options_background('white'); ?>">
    <div class="cb-anchor" id="cb<?= get_row_index() ?>"></div>

    <div class="o-container--wrapper">
        <div class="<?php content_block_options_container('content') ?>">
            <?php if ($form_title) : ?>
                <h3 class="cb-form__title cb__heading-title"><?= $form_title ?></h3>
            <?php endif; ?>

            <?php if ($form_id) : ?>
                <?= do_shortcode('[gravityform id="' . $form_id . '" title="false" description="false" ajax="true"]') ?>
            <?php else: ?>
                <p>No form was selected</p>
            <?php endif; ?>
        </div>
    </div>
</section>
