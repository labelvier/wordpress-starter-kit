<?php $image_data = get_sub_field('image_data');
$image_position = $image_data['image_position'];
$text_distribution = get_sub_field('options')['distribution']; ?>
<section class="cb cb-text-and-image <?php content_block_options_background('white'); ?>">
    <div class="cb-anchor" id="cb<?= get_row_index() ?>"></div>

    <div class="o-container--wrapper">
        <div class="<?php content_block_options_container('content') ?>">
            <div class="o-layout m-image-position--<?= $image_position ?>">
                <div class="o-layout__item u-size-12 <?php if ($text_distribution === 'equal'): echo 'u-size-6@tablet'; elseif ($text_distribution === '23'): echo 'u-size-8@tablet'; elseif ($text_distribution === '34'): echo 'u-size-9@tablet'; endif; ?>">
                    <?php get_template_part('template-parts/blocks/components/content-title-text-buttons') ?>
                </div>
                <div class="o-layout__item u-size-12 <?php if ($text_distribution === 'equal'): echo 'u-size-6@tablet'; elseif ($text_distribution === '23'): echo 'u-size-4@tablet'; elseif ($text_distribution === '34'): echo 'u-size-3@tablet'; endif; ?>">
                    <?php get_template_part('template-parts/blocks/components/content-image') ?>
                </div>
            </div>
        </div>
    </div>
</section>
