<section class="cb cb-text <?php content_block_options_background('white'); ?>">
    <div class="cb-anchor" id="cb<?= get_row_index() ?>"></div>

    <div class="o-container--wrapper">
        <div class="<?php content_block_options_container('content') ?>">
            <?php get_template_part('template-parts/blocks/components/content-title-text-buttons') ?>
        </div>
    </div>
</section>
