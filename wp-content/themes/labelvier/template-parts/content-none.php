<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Labelvier
 */

?>

<section class="no-results not-found">
    <?php if (is_home() && current_user_can('publish_posts')) :
        printf(
            '<p>' . wp_kses(
            /* translators: 1: link to WP admin new post page. */
                __('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'labelvier'),
                array(
                    'a' => array(
                        'href' => array(),
                    ),
                )
            ) . '</p>',
            esc_url(admin_url('post-new.php'))
        );
    elseif (is_search()) : ?>
        <p><?= get_field('pages', 'option')['search_content_not_found'] ?> <span><?= get_search_query() ?></span></p>
        <?php get_search_form();
    else : ?>
        <p>Geen resultaten</p>
    <?php endif; ?>
</section><!-- .no-results -->
