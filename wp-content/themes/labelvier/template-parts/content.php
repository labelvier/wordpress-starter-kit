<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Labelvier
 */

?>

<?php if ( is_singular() ) : ?>

    <div class="wysiwyg-styling">
        <article>
			<?php the_content( sprintf(
				wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading "%s"', 'labelvier' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) ); ?>
        </article>
    </div>

<?php else : ?>

    <?php get_template_part('template-parts/post', false, ['post_type' => get_post_type(), 'post_id' => get_the_ID()]) ?>

<?php endif; ?>
