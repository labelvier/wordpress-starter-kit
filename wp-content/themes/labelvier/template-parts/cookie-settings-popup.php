<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 12-10-2022
 * Time: 15:16
 */
$cookie_settings = get_field( 'cookies_and_scripts', 'option' );
?>
<div class="cookie-settings popup">
    <div class="popup__inner">
        <div class="popup__inner-content wysiwyg-styling">
            <h3><?= $cookie_settings['title'] ?></h3>
            <p><strong><?= $cookie_settings['subtitle'] ?></strong></p>
            <div>
                <?php if ( $lvl2_description = $cookie_settings['level2_tracking_description'] ) : ?>
                    <div class="toggle">
                        <input class="toggle__input" type="checkbox" id="level2_tracking" <?= $_COOKIE['level2Accepted'] ? 'checked' : ''; ?>>
                        <label class="toggle__label" for="level2_tracking">
                            <div class="toggle__label-check"></div>
                            <div class="toggle__label-content">
                                <h5><?= $cookie_settings['level2_tracking_title'] ?></h5>
                                <p><?= $lvl2_description ?></p>
                            </div>
                        </label>
                    </div>
                <?php endif; ?>
                <?php if ( $lvl1_description = $cookie_settings['level1_tracking_description'] ) : ?>
                    <div class="toggle">
                        <input class="toggle__input" type="checkbox" id="level1_tracking" <?= $_COOKIE['level1Accepted'] ? 'checked' : ''; ?>>
                        <label class="toggle__label" for="level1_tracking">
                            <div class="toggle__label-check"></div>
                            <div class="toggle__label-content">
                                <h5><?= $cookie_settings['level1_tracking_title'] ?></h5>
                                <p><?= $lvl1_description ?></p>
                            </div>
                        </label>
                    </div>
                <?php endif; ?>
                <div class="toggle">
                    <input class="toggle__input" type="checkbox" disabled checked>
                    <label class="toggle__label">
                        <div class="toggle__label-check"></div>
                        <div class="toggle__label-content">
                            <h5><?= $cookie_settings['required_tracking_title'] ?></h5>
                            <p><?= $cookie_settings['required_tracking_description'] ?></p>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <div class="popup__inner-actions">
            <a href="#" class="button--clear button blue_trans" onclick="cookieEngine.goBackFromCookiePreferences();return false;"><?= $cookie_settings['cookie_settings_back'] ?></a>
            <button class="button" onclick="cookieEngine.saveCookiePreferences();">
                <?= $cookie_settings['cookie_settings_save'] ?>
            </button>
        </div>

    </div>
</div>
