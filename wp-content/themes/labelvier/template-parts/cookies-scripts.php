<?php
$script_load_placement = [get_query_var( 'script_placement' )];
if($script_load_placement[0] === 'head') {
    // legacy, move scripts to head
    $script_load_placement = ['head', 'body'];
}

if ( $scripts = get_field( 'cookies_and_scripts', 'options' )['scripts'] ?? false ): ?>
    <script>
        window.<?= $script_load_placement[0] ?>Scripts = [];
    </script>
<?php
    foreach ( $scripts as $script ) : the_row();
		if ( in_array( $script['script_placement'], $script_load_placement, true ) ):
            if($script['script_type'] !== 'functional') :
                // escape double quotes and remove line breaks
	            $escaped_script = json_encode( $script['script_code']);
                ?>
                <script>
                    window.<?= $script_load_placement[0] ?>Scripts.push({
                        type: '<?= $script['script_type'] ?>',
                        code: <?= $escaped_script ?>
                    });
                </script>
        <?php
            else:
                echo  $script['script_code'];
            endif;
           endif;
	endforeach;
endif;
