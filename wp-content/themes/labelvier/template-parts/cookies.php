<?php
$cookie_settings = get_field( 'cookies_and_scripts', 'option' );
if ( $cookie_settings['enable_cookie_notice'] ?? false ) : ?>
    <div id="cookie-notice" class="cookie-notice<?= $cookie_settings['enable_cookie_notice'] === '2' ? ' popup' : ' popup popup--bottom-right' ?>">
        <script>
            // hide the notice when the user has accepted the cookies
            if (docCookies.getItem('cookieAccepted')) {
                document.getElementById('cookie-notice').remove();
            }
        </script>
        <div class="popup__inner">
            <div class="popup__inner-content wysiwyg-styling">
				<?= $cookie_settings['cookie_notice_text'] ?>
            </div>
            <div class="popup__inner-actions">
                <button class="button--clean" onclick="cookieEngine.showCookiePreferences();">
                    <?= $cookie_settings['cookie_edit_button'] ?>
                </button>
                <button class="button" onclick="cookieEngine.setAcceptCookie(['level1', 'level2'])">
					<?= $cookie_settings['cookie_notice_button'] ?>
                </button>
            </div>
        </div>
    </div>
    <div id="cookie-settings" class="fullscreen">
        <?php
            // filled via ajax
            // get_template_part( 'template-parts/cookie-settings-popup' );
        ?>
    </div>
<?php endif; ?>
