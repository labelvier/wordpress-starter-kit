<?php
/**
 * Site footer
 * @package Labelvier
 */
?>

<div class="page-footer-inner o-container">
    <?php if (get_field('footer', 'options') !== null):
        if ($footer_columns = get_field('footer', 'options')['columns']) : ?>
            <div class="o-layout@tablet">
                <?php $i = 0;
                foreach ($footer_columns as $footer_column) : $i++;
                    $column_width = $footer_column['column_width'];
                    $set_column_width = '';
                    if ($column_width === '1') {
                        $set_column_width = 'u-size-2@tablet';
                    } elseif ($column_width === '2') {
                        $set_column_width = 'u-size-3@tablet';
                    } elseif ($column_width === '3') {
                        $set_column_width = 'u-size-4@tablet';
                    } elseif ($column_width === '4') {
                        $set_column_width = 'u-size-5@tablet';
                    } elseif ($column_width === '5') {
                        $set_column_width = 'u-size-6@tablet';
                    }
                    ?>
                    <div class="o-layout__item page-footer__column page-footer__column--<?= $i ?> <?= $set_column_width ?>">
                        <?php if ($column_title = $footer_column['title']): ?>
                            <h3 class="page-footer__column-title"><?= $column_title ?></h3>
                        <?php endif; ?>
                        <?php if ($column_list = $footer_column['list']) : ?>
                            <ul>
                                <?php foreach ($column_list as $item):
                                    if (($item['item_mode'] === 'link') && $item_link = $item['link']): ?>
                                        <li><a href="<?= $item_link['url'] ?>"><?= $item_link['title'] ?></a></li>
                                    <?php endif;
                                    if (($item['item_mode'] === 'text') && $item_text = $item['text']): ?>
                                        <li class="wysiwyg-styling"><?= $item_text ?></li>
                                    <?php endif;
                                    if (($item['item_mode'] === 'socials') && $item_socials = $item['socials']): ?>
                                        <?php if ($item_socials_title = $item['socials-title']) : ?>
                                            <li class="socials-title"><?= $item_socials_title ?></li>
                                        <?php endif; ?>
                                        <li class="socials">
                                            <?php foreach ($item_socials as $social) :
                                                if ($social_url = $social['url']) : ?>
                                                    <a href="<?= $social_url ?>" target="_blank" rel="noopener noreferrer" aria-label="Social media link - <?= $social['icon'] ?>">
                                                        <i class="icon-<?= $social['icon'] ?>"></i>
                                                    </a>
                                                <?php endif;
                                            endforeach; ?>
                                        </li>
                                    <?php endif;
                                endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>

<div class="page-footer-bottom o-container">
    <p>&copy; <?= date('Y') ?> <?= get_bloginfo('name') ?></p>
</div>
