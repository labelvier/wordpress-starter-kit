<?php
/**
 * Site header
 * @package Labelvier
 */

$featured_image = null;
$featured_image_default = null;
$featured_image_pages_options = get_field('pages', 'options');

$featured_image_default_options = get_field('general', 'options');
if ($featured_image_default_options && isset($featured_image_default_options['featured_image_default'])) {
    $featured_image_default = $featured_image_default_options['featured_image_default'];
}

if (is_home()) :
    $featured_image = get_post_thumbnail_id(get_queried_object()) ?? $featured_image_default;
elseif (get_field('category_image', get_queried_object())) :
    $featured_image = get_field('category_image', get_queried_object()) ?? $featured_image_default;
elseif (is_archive()) :
    $featured_image = $featured_image_pages_options['featured_image_archives'] ?? $featured_image_default;
elseif (is_search()) :
    $featured_image = $featured_image_pages_options['featured_image_search'] ?? $featured_image_default;
elseif (is_404()) :
    $featured_image = $featured_image_pages_options['featured_image_404'] ?? $featured_image_default;
elseif (get_field('header_image') && get_field('header_image')['image']) :
    $featured_image = get_field('header_image')['image'];
else :
    $featured_image = get_post_thumbnail_id() ?? $featured_image_default;
endif;

$header = get_the_title();
if (is_home()) :
    $header = get_the_title(get_queried_object());
elseif (is_archive()) :
    $header = get_the_archive_title();
elseif (is_404()) :
    $header = get_field('pages', 'options')['404_header'];
elseif (is_search()) :
    $header = get_field('pages', 'options')['search_header'];
endif;

// ACF page header support
if (get_field('header_content') && get_field('header_content')['title']) :
    $header = get_field('header_content')['title'];
endif;

// woocommerce support
if (function_exists('is_woocommerce') && is_product_category()) :
    $featured_image = get_term_meta(get_queried_object_id(), 'thumbnail_id', true);
    $header = get_queried_object()->name;
endif;

$type_header = get_field('type_header') ?? 'default';

?>

<div class="page-header__top">
    <div class="page-header__top-inner">
        <a class="page-header__top-inner__logo" href="<?= esc_url(get_home_url()) ?>" rel="home" itemprop="url">
            <img src="<?= esc_url(get_template_directory_uri()) ?>/dist/images/logo.svg" alt="Logo" itemprop="logo"/>
        </a>

        <?php monkey_menu('main-menu') ?>
    </div>
</div>

<div class="page-header__hero">
    <?php if ($featured_image) : ?>
        <figure class="u-image-background-container">
            <?= wp_get_attachment_image_with_focal_point($featured_image, 'header', false, ['fetchpriority' => 'high']) ?>
        </figure>
    <?php endif; ?>
    <div class="page-header__hero-content">
        <div class="o-container">
            <h1 class="page-header__hero-content__header"><?= $header ?></h1>
        </div>
    </div>
</div>
