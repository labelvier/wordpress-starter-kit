<?php
/**
 * @var array $args
 */
$post_id   = $args['post_id'];
$post_type = $args['post_type'];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post post--hover' ); ?>>
    <a class="post__link" href="<?= esc_url( get_permalink() ) ?>" aria-label="More about <?= the_title() ?>"></a>
    <div class="post__image">
        <figure class="u-image-set-ratio">
            <?= wp_get_attachment_image_with_focal_point( get_post_thumbnail_id(), 'item' ) ?>
        </figure>
    </div>
    <div class="post__content">
        <header>
            <h2 class="post__content-header"><?= the_title() ?></h2>
        </header>
        <div class="post__content-text">
            <?= labelvier_excerpt() ?>
        </div>
        <div class="post__content-link button"><?= esc_html__( 'Lees verder', 'labelvier' ) ?></div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->



