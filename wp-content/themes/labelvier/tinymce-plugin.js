/**
 * Add button to TinyMCE
 */
( function() {
    'use strict';

    tinymce.PluginManager.add('labelvier_tc_button', function( editor, url ) {
        editor.addButton( 'labelvier_tc_button', {
            text: "Button",
            title: "Insert Button",
            onclick: function() {
                editor.windowManager.open( {
                    title: 'Insert Button',
                    width: 500,
                    height: 300,
                    body: [
                        {
                            type: 'textbox',
                            name: 'url',
                            label: 'URL'
                        },
                        {
                            type: 'textbox',
                            name: 'label',
                            label: 'Tekst'
                        },
                        {
                            type: 'listbox',
                            name: 'style',
                            label: 'Stijl',
                            'values': [
                                { text: "Normaal", value: "button" },
                                { text: "Outline", value: "button--outline" },
                            ]
                        },
                        {
                            type: 'checkbox',
                            name: 'newtab',
                            label: ' ',
                            text: 'Open link in een nieuw tabblad',
                            checked: false
                        }],
                    onsubmit: function( e ) {
                        let $content = '<a href="' + e.data.url + '" class="' + (e.data.style !== 'default' ? ' ' + e.data.style : '') + '"' + (!!e.data.newtab ? ' target="_blank"' : '' ) + '>' + e.data.label + '</a>';
                        editor.insertContent( $content );
                    }
                });
            }
        });
    });

} )();

// Check if gutenberg is active
if (typeof wp.blocks !== 'undefined') {
    // Remove button styles from gutenberg
    wp.domReady( () => {
        wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
        wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
        wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
    } );
}

// Save page when pressing Ctrl+S (Windows) or Cmd+S (Mac)
jQuery(document).on('keydown', function(e) {
    if ((e.metaKey || e.ctrlKey) && e.keyCode === 83) {
        e.preventDefault();
        jQuery('#publish').click();
    }
});

// Reset all tabs in flexible content to the first tab
if (jQuery('.acf-field-flexible-content').length) {
    jQuery('.acf-tab-group > li:first-child a').click();
}
