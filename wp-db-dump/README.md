# Starter-kit DB

The files `db.sql` and `db.5.x.sql` are exact copies

`db.sql` is used for the db:import - db:export an `db.5.x.sql` the backup

## db.5.4.sql
This is a clean install based on WP 5.4 without clutter like revisions or dummy content, but with some pre-sets like:

### Theme
- Label Vier theme is activated

### Pages
- Homepage (set as front-page)
- Blog (set as blog page)
- Cookies (uses cookies template with special ACF fields)
- Styleguide (linked to the page-styleguide.php template)

### Menu

Main menu is added and linked to the menu code in the site-header.php template

### Plugins

ACF pro and LabelVier Extras are activated

## How to import / export

`npm run db:import` to import the db.sql to your localhost db. (make sure localhost db is empty)

`npm run db:export` to export your localhost db the db.sql to your localhost db.

When exporting your local db, db.sql will be overwritten. Make copies if needed.
